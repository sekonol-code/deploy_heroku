FROM maven:3.3-jdk-8
ADD target/batm-0.0.1-SNAPSHOT.war /opt/apt.jar
RUN bash -c "touch /opt/apt.jar"
ENTRYPOINT [ "java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/apt.jar" ]
