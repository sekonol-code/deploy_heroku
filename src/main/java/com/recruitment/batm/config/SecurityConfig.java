package com.recruitment.batm.config;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rifki
 */
import javax.sql.DataSource;

import com.recruitment.batm.dao.RolesDao;
import com.recruitment.batm.dao.SubMenuDao;
import com.recruitment.batm.dao.UserAccessMenuDao;
import com.recruitment.batm.dto.SubMenuDto;
import com.recruitment.batm.model.Roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ROLE_USER = "SELECT u.uname, r.role FROM roles r"
    + " JOIN users u ON u.id_role = r.id_role WHERE uname =? group by u.uname, r.role";

    private static final String GET_USER = "SELECT uname, upass, is_active"
    + " from users where uname =? group by uname, upass, is_active";

    @Autowired
    DataSource dataSource;
    
    @Autowired
    private SubMenuDao subMenu;

    @Autowired
    private RolesDao roleDao;
    
    @Autowired
    private UserAccessMenuDao uamDao;
    
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
    

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(encoder())
                .usersByUsernameQuery(GET_USER)
                .authoritiesByUsernameQuery(ROLE_USER);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        List<SubMenuDto> sm = new ArrayList<SubMenuDto>();

        List<Roles> listRole = (ArrayList<Roles>)roleDao.findAll();
        for(Roles r : listRole){
            SubMenuDto temp = new SubMenuDto();
            temp.setRole(r.getRole());
            List<String> submenuList = new ArrayList<String>();
            List<Integer> menuList = uamDao.findByIdRole(r.getId_role());
            for(int i : menuList){
                List<String> submenu = subMenu.findByIdMenu(i);
                submenuList.addAll(submenu);
            }
            temp.setUrl_subMenu(submenuList);
            sm.add(temp);
        }

        for (SubMenuDto matcher : sm) {
            for(String url:matcher.getUrl_subMenu()){
             http.authorizeRequests().antMatchers(url+"/**").hasAnyAuthority(matcher.getRole(),"Admin");
           }
         }
       
        http
        .authorizeRequests()
        .antMatchers("/", "/forget-password/**").permitAll()
                .antMatchers(
                    "/login",
                    "/register/**", 
                     "/css/**", 
                     "/js/**",
                     "/lib/**",
                     "/admin/**",
                     "/contactform/**",
                     "/js_new/**", 
                     "/img/**",
                     "/datatables/**",
                     "/applicants/create/**",
                     "/home/**",
                     "/vendor/**", 
                     "/**/favicon.png",
                    "/webjars/**", "/signup").permitAll()
                    .and()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login?error=true")
                .defaultSuccessUrl("/admin", true)
                .and()
                .rememberMe()
                .rememberMeCookieName("cookies-remember-me")
                .tokenValiditySeconds(259200)
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .deleteCookies("cookies-remember-me")
                .logoutSuccessUrl("/index");



                //https://stackoverflow.com/questions/31704593/spring-security-authorize-requests-value-from-database

                
    }

}
