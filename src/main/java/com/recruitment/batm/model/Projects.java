package com.recruitment.batm.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.recruitment.batm.validator.ValidateDateRange;

import lombok.Data;

@Entity
@Data
@Table(name = "projects")
@ValidateDateRange(start="date_awal", end="date_akhir")
public class Projects{


    @Id
    @SequenceGenerator(name = "projects_id_project_seq", sequenceName = "projects_id_project_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projects_id_project_seq") 
    private int id_project;

    @NotNull
    private int id_ap;

    @NotNull @NotEmpty
    private String nama_project;

    @NotNull @NotEmpty
    private String posisi_project;

    @NotNull
    private Date date_awal;

    @NotNull
    private Date date_akhir;

}