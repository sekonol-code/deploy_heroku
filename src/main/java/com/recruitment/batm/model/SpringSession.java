package com.recruitment.batm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "spring_session")
public class SpringSession {
    @Id
    @Column(name = "PRIMARY_ID")
    private char PRIMARY_ID;

    @NotNull @NotEmpty
    @Column(name = "SESSION_ID")
    private String SESSION_ID;

    @NotNull
    @Column(name = "CREATION_TIME")
    private long CREATION_TIME;
    
    @NotNull
    @Column(name = "LAST_ACCESS_TIME")
    private long LAST_ACCESS_TIME;
    
    @NotNull
    @Column(name = "MAX_INACTIVE_INTERVAL")
    private int MAX_INACTIVE_INTERVAL;
    
    @NotNull
    @Column(name = "EXPIRY_TIME")
    private long EXPIRY_TIME;

    @NotNull @NotEmpty
    @Column(name = "PRINCIPAL_NAME")
    private String PRINCIPAL_NAME;
}
