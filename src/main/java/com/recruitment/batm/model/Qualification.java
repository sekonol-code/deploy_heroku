package com.recruitment.batm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
@Table(name = "qualification")
public class Qualification{
    @Id
    @SequenceGenerator(name = "qualification_id_qualification_seq", sequenceName = "qualification_id_qualification_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qualification_id_qualification_seq")
    private int id_qualification;

    @NotNull
    private int id_job;

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String nama_qualification;
}