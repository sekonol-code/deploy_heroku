package com.recruitment.batm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
@Table(name = "description")
public class Description{


    @Id 
    @SequenceGenerator(name = "description_id_desc_seq", sequenceName = "description_id_desc_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "description_id_desc_seq")
    private int id_desc;

    @NotNull
    private int id_job;

    @NotNull @NotEmpty @Size(max = 255)
    private String nama_desc;

}

