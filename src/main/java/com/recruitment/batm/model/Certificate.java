package com.recruitment.batm.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
@Table(name = "certificate")
public class Certificate{

    @Id
    @SequenceGenerator(name = "certificate_id_certificate_seq", sequenceName = "certificate_id_certificate_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "certificate_id_certificate_seq")
    private int id_certificate;

    @NotNull
    private int id_ap;

    @NotNull @NotEmpty
    private String nama_certificate;

    @NotNull
    private Date date;

    @NotNull @NotEmpty
    private String company;

    private String description;
}