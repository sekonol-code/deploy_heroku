package com.recruitment.batm.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.recruitment.batm.validator.ValidateDateRange;

import lombok.Data;

@Data
@Entity
@Table(name = "training")
@ValidateDateRange(start="date_awal", end="date_akhir")
public class Training {
    
    @Id
    @SequenceGenerator(name = "training_id_training_seq", sequenceName = "training_id_training_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "training_id_training_seq")
    private int id_training;

    @NotNull 
    private int id_ap;

    @NotNull @NotEmpty
    private String ntraining;
    
    @NotNull 
    private Date date_awal;
    
    @NotNull
    private Date date_akhir;

    private String description;

    @NotNull @NotEmpty
    private String company;
}