package com.recruitment.batm.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.recruitment.batm.validator.ValidateDateRange;

import lombok.Data;

@Entity
@Data
@Table(name = "educations")
@ValidateDateRange(start="date_awal", end="date_akhir")
public class Educations {

    @Id
    @SequenceGenerator(name = "educations_id_education_seq", sequenceName = "educations_id_education_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "educations_id_education_seq")
    private int id_education;

    @NotNull
    private int id_ap;

    @NotNull
    private Date date_awal;

    @NotNull
    private Date date_akhir;

    @NotEmpty
    @NotNull
    private String school;

    @NotEmpty
    @NotNull
    private String major;

    private String description;
}