package com.recruitment.batm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
@Table(name = "skills")
public class Skills{
    
    @Id
    @SequenceGenerator(name = "skills_id_skill_seq", sequenceName = "skills_id_skill_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "skills_id_skill_seq")
    private int id_skill;

    @NotNull
    private int id_ap;

    @NotNull @NotEmpty
    private String skill;

    @NotNull @NotEmpty
    private String level;
}