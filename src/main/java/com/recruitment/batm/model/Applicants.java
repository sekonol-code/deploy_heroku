package com.recruitment.batm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
@Table(name = "applicants")
public class Applicants{
    
    @Id
    @SequenceGenerator(name = "applicants_id_ap_seq", sequenceName = "applicants_id_ap_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "applicants_id_ap_seq")
    private int id_ap;

    @NotEmpty @NotNull
    private String nama;

    @NotEmpty @NotNull
    private String alamat;

    @NotEmpty @NotNull @Email
    private String email;

    @NotEmpty @NotNull
    private String contact;

    @NotEmpty @NotNull
    private String personal_statement;

    private byte[] data_gambar;

    private String link_gambar;

    private byte[] data_cv;

    private String link_cv;
}