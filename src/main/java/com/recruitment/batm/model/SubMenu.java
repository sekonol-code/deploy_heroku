package com.recruitment.batm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table (name = "submenu")
public class SubMenu{

    @Id
    private int id_sub_menu;

    @NotNull
    @Column(name = "id_menu")
    private int id_menu;

    @NotNull
    @NotEmpty
    @Column(name = "nama_sub_menu")
    private String nama_sub_menu;

    @Column(name = "url")
    private String url;

    @Column(name = "icon")
    private String icon;

    @Column(name = "is_active")
    private int is_active;

}