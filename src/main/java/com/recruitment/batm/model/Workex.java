package com.recruitment.batm.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.recruitment.batm.validator.ValidateDateRange;

import lombok.Data;

@Data
@Entity
@Table(name = "workex")
@ValidateDateRange(start="date_awal", end="date_akhir")
public class Workex {
    
    @Id
    @SequenceGenerator(name = "workex_id_workex_seq", sequenceName = "workex_id_workex_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "workex_id_workex_seq")
    private int id_workex;

    @NotNull 
    private int id_ap;
    
    @NotNull
    private Date date_awal;
    
    @NotNull
    private Date date_akhir;

    @NotNull @NotEmpty
    private String job;

    @NotNull @NotEmpty
    private String description;

    @NotNull @NotEmpty
    private String company;
}