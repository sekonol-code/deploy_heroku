package com.recruitment.batm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "menu")

public class Menu{

    @Id
    private int id_menu;

    @NotNull
    @NotEmpty
    @Column(name = "nama_menu")
    private String nama_menu;

    @Column(name = "is_active")
    private String is_active;
}