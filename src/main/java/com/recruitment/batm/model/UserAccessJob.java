package com.recruitment.batm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
@Table(name = "applicants_access_job")
public class UserAccessJob{
    
    @Id
    @SequenceGenerator(name = "applicants_access_job_id_applicants_access_job_seq", sequenceName = "applicants_access_job_id_applicants_access_job_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "applicants_access_job_id_applicants_access_job_seq")
    private int id_applicants_access_job;

    @NotNull
    private int id_ap;

    @NotNull
    private int id_job;

    @NotNull
    private int status;
}