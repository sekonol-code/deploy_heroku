package com.recruitment.batm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
@Table(name = "job")
public class Job{


    @Id
    @SequenceGenerator(name = "job_id_job_seq", sequenceName = "job_id_job_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "job_id_job_seq")
    private int id_job;

    @NotNull @NotEmpty
    private String nama_job;

    private int is_active;
}
