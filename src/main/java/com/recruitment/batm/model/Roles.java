package com.recruitment.batm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "roles")
public class Roles {
    @Id
    @Column(columnDefinition = "serial")
    private int id_role;

    @NotNull @NotEmpty
    @Column(name = "role")
    public String role;

}
