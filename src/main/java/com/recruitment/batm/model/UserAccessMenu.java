package com.recruitment.batm.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table (name = "users_access_menu")
public class UserAccessMenu {
    @Id
    private int id_user_access_menu;

    @NotNull
    private int id_role;

    @NotNull
    private int id_menu;
}
