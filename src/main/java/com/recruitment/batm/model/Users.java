package com.recruitment.batm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class Users {
    @Id
    private int id_users;

    @NotNull 
    @Column(name = "id_role")
    private int id_role;

    @NotNull @NotEmpty
    @Column(name = "uname")
    private String uname;

    @NotNull @NotEmpty
    @Column(name = "email")
    private String email;

    @NotNull @NotEmpty
    @Column(name = "upass")
    private String upass;

    @NotNull 
    @Column(name = "is_active")
    private int is_active;
}