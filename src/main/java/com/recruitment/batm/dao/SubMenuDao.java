package com.recruitment.batm.dao;

import java.util.List;

//import com.recruitment.batm.dto.SubMenuDto;
import com.recruitment.batm.model.SubMenu;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

//public interface SubMenuDao extends PagingAndSortingRepository<SubMenuDto, Integer>
public interface SubMenuDao extends PagingAndSortingRepository<SubMenu, Integer>{
    
    // @Query("SELECT DISTINCT r.role FROM submenu s JOIN menu m ON s.id_menu = m.id_menu JOIN user_access_menu uam ON  m.id_menu = uam.id_menu JOIN roles r ON uam.id_role = r.id_role JOIN [user] u ON r.id_role = u.id_role WHERE u.uname = ?1 group by u.uname, u.upass, u.is_active, r.role, s.url")
    // List<Integer> findRole(String role);

    @Query("SELECT sm.url FROM SubMenu sm WHERE sm.id_menu = ?1")
    List<String> findByIdMenu(int id_menu);
}
