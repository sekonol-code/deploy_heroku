package com.recruitment.batm.dao;

import java.util.List;


import com.recruitment.batm.model.Applicants;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface ApplicantsDao extends PagingAndSortingRepository<Applicants, Integer>{
    @Query("select ap.id_ap from Applicants ap where ap.id_ap=?1")
    List<Applicants> findApplicantsById(int id_ap);

}