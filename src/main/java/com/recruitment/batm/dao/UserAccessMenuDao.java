package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.UserAccessMenu;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserAccessMenuDao extends PagingAndSortingRepository<UserAccessMenu, Integer>{
    @Query("SELECT uam.id_menu FROM UserAccessMenu uam WHERE uam.id_role = ?1")
    List<Integer> findByIdRole(int role);

}
