package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Qualification;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author delapan
 */
public interface QualificationDao extends PagingAndSortingRepository<Qualification,Integer>{
    @Query("Select q from Qualification q where q.id_job = ?1")            
    List<Qualification> findQualJob(Integer id_job);
}
