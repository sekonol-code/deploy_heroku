package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Applicants;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ExportDao extends PagingAndSortingRepository<Applicants, Integer> {

    @Query("SELECT a.id_ap FROM UserAccessJob uaj join Applicants a on uaj.id_ap = a.id_ap where uaj.id_job = ?1 AND uaj.status = ?2")
    List<Integer> jobStatus(int job, int status);


  
}
