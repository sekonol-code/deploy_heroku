package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Projects;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProjectsDao extends PagingAndSortingRepository<Projects, Integer> {
    
    @Query("select pr from Projects pr where pr.id_ap=?1 order by pr.date_akhir desc")
    List<Projects> findProjectsByIdAp(int id_ap);

}