package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Workex;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WorkexDao extends PagingAndSortingRepository<Workex, Integer> {

    @Query("select we from Workex we where we.id_ap=?1 and we.date_akhir IN (select max(we.date_akhir) from Workex we where we.id_ap=?1)")
    List<Workex> findTopWorkexByIdAp(int id_ap);

    @Query("select we from Workex we where we.id_ap=?1 order by we.date_akhir desc")
    List<Workex> findWorkexByIdAp(int id_ap);

    // Year
    @Query(value = "SELECT CAST((DATE_PART('year',  max(we.date_akhir)) - DATE_PART('year', min(we.date_awal)))* 12 + (DATE_PART('month',  max(we.date_akhir)) - DATE_PART('month', min(we.date_awal))) AS INTEGER) / 12 FROM Workex we WHERE we.id_ap = ?1", nativeQuery = true)
    Integer findYearDif(int id_ap);

    // Month
    @Query(value = "SELECT CAST((DATE_PART('year',  max(we.date_akhir)) - DATE_PART('year', min(we.date_awal)))* 12 + (DATE_PART('month',  max(we.date_akhir)) - DATE_PART('month', min(we.date_awal))) AS INTEGER) % 12 FROM Workex we WHERE we.id_ap = ?1", nativeQuery = true)
    Integer findMonthDif(int id_ap);

}