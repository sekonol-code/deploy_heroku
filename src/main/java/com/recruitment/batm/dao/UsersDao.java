package com.recruitment.batm.dao;



import com.recruitment.batm.model.Users;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UsersDao extends PagingAndSortingRepository<Users, Integer>{
    @Query("select u from Users u where u.uname=?1 ")
    Users findUsersByUname(String uname);
}