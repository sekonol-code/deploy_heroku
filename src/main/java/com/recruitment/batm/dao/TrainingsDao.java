package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Training;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TrainingsDao extends PagingAndSortingRepository<Training, Integer> {
    @Query("select t from Training t where t.id_ap=?1")
    List<Training> findTrainingById(int id_ap);
}