package com.recruitment.batm.dao;


import java.util.List;



import com.recruitment.batm.model.UserAccessJob;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserAccessJobDao extends PagingAndSortingRepository<UserAccessJob, Integer> {
   
    @Query("select uaj from UserAccessJob uaj join Job j on uaj.id_job = j.id_job where uaj.id_applicants_access_job= ?1 and uaj.status = ?2")
    UserAccessJob findByIdUajStatus(int id_user_access_job, int status);

    // @Query("select ap.id_ap from Applicants ap join UserAccessJob uaj on uaj.id_ap = ap.id_ap where uaj.id_job = ?1 and uaj.status = ?2")
    // List<Integer> findApplicants(int id_job, int status);

    @Query("select uaj from UserAccessJob uaj join Applicants ap on ap.id_ap = uaj.id_ap where uaj.id_ap = ?1")
    List<UserAccessJob> findUajByIdApplicants(int id_ap);


    @Query("select uaj from UserAccessJob uaj join Job j on uaj.id_job = j.id_job where uaj.id_job = ?1 and uaj.status = ?2")
    List<UserAccessJob> findByJobStatus(int id_job, int status);

  // @Query("select ap from Applicants ap join Educations edu on ap.id_ap = edu.id_ap join UserAccessJob uaj on uaj.id_ap=ap.id_ap join Job j on  uaj.id_job = j.id_job where uaj.id_job = ?1 and uaj.status= ?2")
  //  Iterable<UserAccessJob> findByJobnStatus(int id_job, int status);
  @Query("select uaj from UserAccessJob uaj where uaj.id_job = ?1 and uaj.status= ?2")
    List<UserAccessJob> findByJobnStatus(int id_job, int status);

   
    @Modifying
    @Query("update UserAccessJob uaj set uaj.status = uaj.status+1 where uaj.id_ap=?1")
    Integer updateStatus(int id_ap);

    @Query(" SELECT COUNT(uaj.id_applicants_access_job) as iduaj FROM UserAccessJob uaj WHERE uaj.status = 1 and uaj.id_job=?1")
    int jumlahAplicans(int job);

    @Query(" SELECT COUNT(uaj.id_applicants_access_job) as iduaj FROM UserAccessJob uaj WHERE uaj.status = 2 and uaj.id_job=?1")
    int jumlahShortlist(int job);

    @Query(" SELECT COUNT(uaj.id_applicants_access_job) as iduaj FROM UserAccessJob uaj WHERE uaj.status = 3 and uaj.id_job=?1")
    int jumlahInterview(int job);

    @Query(" SELECT COUNT(uaj.id_applicants_access_job) as iduaj FROM UserAccessJob uaj WHERE uaj.status = 4 and uaj.id_job=?1")
    int jumlahOffered(int job);

    @Query(" SELECT COUNT(uaj.id_applicants_access_job) as iduaj FROM UserAccessJob uaj WHERE uaj.status = 5 and uaj.id_job=?1")
    int jumlahHired(int job);

    @Query(" SELECT COUNT(uaj.id_applicants_access_job) as iduaj FROM UserAccessJob uaj WHERE uaj.status = 6 and uaj.id_job=?1")
    int jumlahArchive(int job);

    @Query(" SELECT COUNT(uaj.id_applicants_access_job) as iduaj FROM UserAccessJob uaj WHERE uaj.status = ?1 and uaj.id_job=?2")
    int jumlahPendaftar(int status, int job);

}