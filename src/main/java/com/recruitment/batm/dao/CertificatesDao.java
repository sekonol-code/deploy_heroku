package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Certificate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CertificatesDao extends PagingAndSortingRepository<Certificate, Integer>{

    @Query("SELECT c FROM Certificate c WHERE id_ap = ?1")
    List<Certificate> findApplicanCertificates(int id_ap);
    
}
