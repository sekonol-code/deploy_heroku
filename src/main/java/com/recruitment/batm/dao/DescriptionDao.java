package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Description;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author delapan
 */
public interface DescriptionDao extends PagingAndSortingRepository<Description,Integer>{
    @Query("Select d from Description d where d.id_job = ?1")            
    List<Description> findDescJob(Integer id_job);
}
