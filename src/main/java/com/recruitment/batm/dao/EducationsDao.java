package com.recruitment.batm.dao;


import java.util.List;

import com.recruitment.batm.model.Educations;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EducationsDao extends PagingAndSortingRepository<Educations, Integer>{
    @Query("select edu from Educations edu where edu.id_ap=?1 order by edu.date_akhir desc")
    List<Educations> findEducationsByIdAp(int id_ap);

    @Query("select edu from Educations edu where edu.id_ap=?1 and edu.date_akhir IN (select max(edu.date_akhir) from Educations edu where edu.id_ap=?1)")
    List<Educations> findTopEducationsByIdAp(int id_ap);

    
}

