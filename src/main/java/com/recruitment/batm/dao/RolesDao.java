package com.recruitment.batm.dao;

import com.recruitment.batm.model.Roles;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface RolesDao extends PagingAndSortingRepository<Roles, Integer>{

}
