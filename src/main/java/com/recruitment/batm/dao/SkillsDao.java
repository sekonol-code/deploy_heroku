package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Skills;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SkillsDao extends PagingAndSortingRepository<Skills, Integer>{

    @Query("SELECT s FROM Skills s WHERE s.id_ap = ?1")
    List<Skills> findApplicanSkills(int id_ap);
    
}
