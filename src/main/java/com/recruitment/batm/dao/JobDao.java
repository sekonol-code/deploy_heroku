package com.recruitment.batm.dao;

import java.util.List;

import com.recruitment.batm.model.Job;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface JobDao extends PagingAndSortingRepository<Job, Integer> {

    @Query("SELECT j FROM Job j WHERE j.id_job != 1 ORDER BY  j.is_active DESC")
    List<Job> findJobExceptGDP();

    @Query("select j from Job j where j.id_job = ?1")
    Job findJobById(int id_ap);

    @Query("SELECT j from Job j where j.is_active = 1")
    List<Job> findActiveJob();

    @Query("SELECT j from Job j where j.is_active = 1 AND j.id_job != 1")
    List<Job> findActiveJobExceptGDP();


}