package com.recruitment.batm.dao;

import com.recruitment.batm.model.SpringSession;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SpringSessionDao extends PagingAndSortingRepository<SpringSession, String>{

    @Query("select s from SpringSession s where s.SESSION_ID = ?1")
    SpringSession findsession(String masuk);

}
