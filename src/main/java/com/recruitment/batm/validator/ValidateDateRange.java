package com.recruitment.batm.validator;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


import javax.validation.Constraint;

@Target({TYPE, ANNOTATION_TYPE })
@Constraint(validatedBy = DateRangeValidator.class)
@Retention(RUNTIME)
@Documented
public @interface ValidateDateRange {
 
    String message() default "End Date should be later than Start Date";
    String start();
    String end();
    Class[] groups() default {};
    Class[] payload() default {}; 
}