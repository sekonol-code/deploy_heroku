package com.recruitment.batm.dto;

import java.util.List;

import javax.validation.Valid;

import com.recruitment.batm.model.Description;
import com.recruitment.batm.model.Job;
import com.recruitment.batm.model.Qualification;

import lombok.Data;

@Data
public class JobDTO {

    @Valid
    private Job job;

    @Valid
    private List<Description> descriptionList;

    @Valid
    private List<Qualification> qualificationList;
}