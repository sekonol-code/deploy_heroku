package com.recruitment.batm.dto;

import java.util.List;

import javax.validation.Valid;

import com.recruitment.batm.model.Applicants;
import com.recruitment.batm.model.Certificate;
import com.recruitment.batm.model.Educations;
import com.recruitment.batm.model.Projects;
import com.recruitment.batm.model.Skills;
import com.recruitment.batm.model.Training;
import com.recruitment.batm.model.Workex;

import lombok.Data;

@Data
public class ApplicantsDto {
    @Valid
    private Applicants applicants;

    @Valid
    private List<Educations> educations;

    @Valid
    private List<Workex> workexs;

    @Valid
    private List<Training> trainings;

    @Valid
    private List<Skills> skills;

    @Valid
    private List<Certificate> certificates;

    @Valid
    private List<Projects> projects;
    
}