package com.recruitment.batm.dto;

import java.util.List;

import lombok.Data;

@Data
public class SubMenuDto  {
    public String role;
    public List<String> url_subMenu;
}
