package com.recruitment.batm.controller;

import java.util.ArrayList;
import java.util.List;

import com.recruitment.batm.dao.ApplicantsDao;
import com.recruitment.batm.dao.CertificatesDao;
import com.recruitment.batm.dao.EducationsDao;
import com.recruitment.batm.dao.ExportDao;
import com.recruitment.batm.dao.ProjectsDao;
import com.recruitment.batm.dao.SkillsDao;
import com.recruitment.batm.dao.TrainingsDao;
import com.recruitment.batm.dao.WorkexDao;
import com.recruitment.batm.dto.ApplicantsDto;
import com.recruitment.batm.model.Skills;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DownloadController {

    @Autowired
    private ApplicantsDao applicanDao;

    @Autowired
    private EducationsDao educationDao;

    @Autowired
    private WorkexDao workexDao;

    @Autowired
    private SkillsDao skillsDao;
    
    @Autowired
    private TrainingsDao trainingsDao;
    
    @Autowired
    private CertificatesDao certificatesDao;

    @Autowired
    private ProjectsDao projectsDao;

    @Autowired
    private ExportDao exportDao;

    @Autowired
    private EducationsDao educationsDao;

    @Autowired
    private ApplicantsDao applicantsDao;
    
    //Generate CV
    @RequestMapping("index/detail/{id_job}/{status}/download/pdf/{id_ap}")
    public String showApplicantsById(@PathVariable int id_job, @PathVariable int status, @PathVariable int id_ap, Model model){
        model.addAttribute("applicants", applicanDao.findById(id_ap).get());
        model.addAttribute("education", educationDao.findEducationsByIdAp(id_ap));
        model.addAttribute("workex", workexDao.findWorkexByIdAp(id_ap));
        model.addAttribute("skills", skillsDao.findApplicanSkills(id_ap));
        model.addAttribute("trainings", trainingsDao.findTrainingById(id_ap));
        model.addAttribute("projects", projectsDao.findProjectsByIdAp(id_ap));
        model.addAttribute("certificates", certificatesDao.findApplicanCertificates(id_ap));
        return "printcv";
    }

    //Generate table
    @GetMapping("index/detail/{id_job}/{status}/export/download")
    public String downloadTable(@PathVariable int id_job, @PathVariable int status, Model model) {
        List<ApplicantsDto> applicantsDto = new ArrayList<ApplicantsDto>();
        List<Integer> yearDiffList = new ArrayList<Integer>();
        List<Integer> monthDiffList = new ArrayList<Integer>();
        List<Integer> company = new ArrayList<Integer>();
        List<Integer> idList = exportDao.jobStatus(id_job, status);
        List<String> skillStrings = new ArrayList<String>();
        for (Integer i : idList) {
            ApplicantsDto temp = new ApplicantsDto();
            temp.setApplicants(applicantsDao.findById(i).get());
            temp.getApplicants().setAlamat(temp.getApplicants().getAlamat().replace(',', '.'));
            temp.setEducations(educationsDao.findEducationsByIdAp(i));
            if(workexDao.findWorkexByIdAp(i) != null){
                temp.setWorkexs(workexDao.findWorkexByIdAp(i));
            }
            temp.setSkills(skillsDao.findApplicanSkills(i));
            applicantsDto.add(temp);
            String s = "";
            for (Skills a : temp.getSkills()) {
                s += a.getSkill().concat("  ");
            }
            skillStrings.add(s);
            int yearDiff = 0;
            if(workexDao.findYearDif(i) != null){
                yearDiff = workexDao.findYearDif(i);
            }
            int monthDiff = 0;
            if(workexDao.findMonthDif(i) != null ){
                monthDiff = workexDao.findMonthDif(i);
            }
            yearDiffList.add(yearDiff);
            monthDiffList.add(monthDiff);
        }
        model.addAttribute("skillStrings", skillStrings);
        model.addAttribute("company", company);
        model.addAttribute("yearDiff", yearDiffList);
        model.addAttribute("monthDiff", monthDiffList);
        model.addAttribute("applicants", applicantsDto);

        
        return "export";
    }

}
