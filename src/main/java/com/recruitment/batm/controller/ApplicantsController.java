package com.recruitment.batm.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.recruitment.batm.dao.ApplicantsDao;
import com.recruitment.batm.dao.CertificatesDao;
import com.recruitment.batm.dao.EducationsDao;
import com.recruitment.batm.dao.JobDao;
import com.recruitment.batm.dao.ProjectsDao;
import com.recruitment.batm.dao.SkillsDao;
import com.recruitment.batm.dao.SpringSessionDao;
import com.recruitment.batm.dao.TrainingsDao;
import com.recruitment.batm.dao.UserAccessJobDao;
import com.recruitment.batm.dao.UsersDao;
import com.recruitment.batm.dao.WorkexDao;
import com.recruitment.batm.dto.ApplicantsDto;
import com.recruitment.batm.model.Applicants;
import com.recruitment.batm.model.Certificate;
import com.recruitment.batm.model.Educations;
import com.recruitment.batm.model.Job;
import com.recruitment.batm.model.Projects;
import com.recruitment.batm.model.Skills;
import com.recruitment.batm.model.SpringSession;
import com.recruitment.batm.model.Training;
import com.recruitment.batm.model.UserAccessJob;
import com.recruitment.batm.model.Users;
import com.recruitment.batm.model.Workex;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Controller
public class ApplicantsController {

    @Autowired
    private ApplicantsDao aDao;

    @Autowired
    private EducationsDao eduDao;

    @Autowired
    private SkillsDao skillsDao;

    @Autowired
    private TrainingsDao trainDao;

    @Autowired
    private WorkexDao workDao;

    @Autowired
    private CertificatesDao certDao;

    @Autowired
    private ProjectsDao projDao;

    @Autowired
    private UserAccessJobDao uajDao;

    @Autowired
    private JobDao jobDao;

    @Autowired
    SpringSessionDao springSessionDao;

    @Autowired
    UsersDao userDao;

    @GetMapping("/applicants/create/{id_job}")
    public String showForm(Model model, @PathVariable int id_job, HttpSession session) {
        ApplicantsDto newForm = new ApplicantsDto();
        newForm.setSkills(new ArrayList<Skills>());
        model.addAttribute("applicantsDto", newForm);
        UserAccessJob uaj = new UserAccessJob();
        uaj.setId_job(id_job);
        uaj.setStatus(1);
        model.addAttribute("uaj", uaj);

        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        try {
            String namauser = sesion.getPRINCIPAL_NAME();
        model.addAttribute("userm", userDao.findUsersByUname(namauser));
        return "formUser";    
        } catch (NullPointerException e) {
            return "formUser"; 
        }
        
    }

    @GetMapping("/index/detail/{id_job}/1/applicants/createnew")
    public String showFormadd(Model model, @PathVariable int id_job, HttpSession session) {
        ApplicantsDto newForm = new ApplicantsDto();
        model.addAttribute("applicantsDto", newForm);
        UserAccessJob uaj = new UserAccessJob();
        uaj.setStatus(1);
        model.addAttribute("uaj", uaj);

        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        Users u = userDao.findUsersByUname(namauser);

        model.addAttribute("userm", u);
        try {
            if (u.getId_role() == 1) {
                Job jobactive = jobDao.findJobById(1);
                model.addAttribute("jobList", jobactive);
                return "formUser";

            } else if (u.getId_role() == 2) {

                List<Job> jobactive = jobDao.findActiveJobExceptGDP();
                model.addAttribute("jobList", jobactive);
                return "formUser";
            } else {
                List<Job> jobactive = jobDao.findActiveJob();
                model.addAttribute("jobList", jobactive);
                return "formUser";
            }
        } catch (Exception e) {
            return "formUser";
        }

    }

    @PostMapping("/applicants/create")
    public String create(Model model, @Valid @ModelAttribute("applicantsDto") ApplicantsDto applicantsForm,
            BindingResult bindingResult, @RequestParam("file") MultipartFile[] files,
            @ModelAttribute("uaj") UserAccessJob uaj, BindingResult uajResult, RedirectAttributes redirectAttributes,
            HttpSession session) {
        // System.out.println("kakaka "+bindingResult.hasErrors());
        if (bindingResult.hasErrors()) {
            System.out.println("aaaaa " + bindingResult);
            return "formUser";
        }

        Applicants p = applicantsForm.getApplicants();
        Applicants ap = aDao.save(p);
        int id = ap.getId_ap();
        for (MultipartFile multi : files) {
            if (multi.getContentType().toString().contains("application/octet-stream") && !multi.isEmpty()) {

                try {
                    ap.setData_gambar(multi.getBytes());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("" + id)
                        .path("/img/").path("foto.jpg").toUriString();
                ap.setLink_gambar(fileDownloadUri);
            } else if (multi.getContentType().toString().equals("application/pdf") && !multi.isEmpty()) {
                try {
                    ap.setData_cv(multi.getBytes());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("" + id)
                        .path("/pdf/").path("cv.pdf").toUriString();
                ap.setLink_cv(fileDownloadUri);
            }
        }

        uaj.setId_ap(id);
        uajDao.save(uaj);

        model.addAttribute("applicants", aDao.save(ap));

        List<Skills> sList = applicantsForm.getSkills();
        model.addAttribute("skills", sList);
        for (Skills e : sList) {
            e.setId_ap(id);
            skillsDao.save(e);
        }

        List<Educations> eduList = applicantsForm.getEducations();
        model.addAttribute("educations", eduList);
        for (Educations e : eduList) {
            e.setId_ap(id);
            eduDao.save(e);
        }

        List<Training> tList = applicantsForm.getTrainings();
        model.addAttribute("trainings", tList);
        if (tList != null) {
            for (Training e : tList) {
                e.setId_ap(id);
                trainDao.save(e);
            }
        }

        List<Workex> wList = applicantsForm.getWorkexs();
        model.addAttribute("workexs", wList);
        if (wList != null) {
            for (Workex e : wList) {
                e.setId_ap(id);
                workDao.save(e);
            }
        }

        List<Certificate> cList = applicantsForm.getCertificates();
        model.addAttribute("certificates", cList);
        if (cList != null) {
            for (Certificate e : cList) {
                e.setId_ap(id);
                certDao.save(e);
            }
        }

        List<Projects> pList = applicantsForm.getProjects();
        model.addAttribute("projects", pList);
        if (cList != null) {
            for (Projects e : pList) {
                e.setId_ap(id);
                projDao.save(e);
            }
        }

        redirectAttributes.addFlashAttribute("message", "Success");

        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        Users u = userDao.findUsersByUname(namauser);
        if (namauser == null) {

            return "redirect:/";

        } else if (u.getId_role()==1) {
            return "redirect:/index2";
        }
        else {
            return "redirect:/index";
        } 

    }

    @GetMapping("/{id}/img/foto.jpg")
    public ResponseEntity downloadImgAnakFromDB(@PathVariable String id) {
        Optional<Applicants> op = aDao.findById(Integer.parseInt(id));
        Applicants p = op.get();
        return ResponseEntity.ok().contentType(MediaType.parseMediaType("image/jpg"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "foto.jpg" + "\"")
                .body(p.getData_gambar());
    }

    @GetMapping("/{id}/pdf/cv.pdf")
    public ResponseEntity downloadPdfAnakFromDB(@PathVariable String id) {
        Optional<Applicants> op = aDao.findById(Integer.parseInt(id));
        Applicants p = op.get();
        return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "cv.pdf" + "\"")
                .body(p.getData_cv());
    }

    @GetMapping("/index/detail/{id_job}/{status}/edit/{id_ap}")
    public String editFromAdmin(Model model, @PathVariable int id_ap, @PathVariable int id_job,
            @PathVariable int status, HttpSession session) {
        ApplicantsDto editForm = new ApplicantsDto();
        editForm.setApplicants(aDao.findById(id_ap).get());
        editForm.setSkills(skillsDao.findApplicanSkills(id_ap));
        editForm.setEducations(eduDao.findEducationsByIdAp(id_ap));
        editForm.setWorkexs(workDao.findWorkexByIdAp(id_ap));
        editForm.setCertificates(certDao.findApplicanCertificates(id_ap));
        editForm.setTrainings(trainDao.findTrainingById(id_ap));
        editForm.setProjects(projDao.findProjectsByIdAp(id_ap));
        model.addAttribute("applicantsDto", editForm);
        model.addAttribute("uaj", uajDao.findUajByIdApplicants(id_ap).get(0));
        
        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        Users u = userDao.findUsersByUname(namauser);

        model.addAttribute("userm", u);
        return "formUser";
    }

    @GetMapping("/index/detail/{id_job}/{status}/{id_ap}/delete/skill/{id}")
    public String deleteSkill(@PathVariable int id, @PathVariable int id_ap, @PathVariable int id_job,
            @PathVariable int status) {
        skillsDao.deleteById(id);
        return "redirect:/index/detail/" + id_job + "/" + status + "/edit/" + id_ap;
    }

    @GetMapping("/index/detail/{id_job}/{status}/{id_ap}/delete/education/{id}")
    public String deleteEdu(@PathVariable int id, @PathVariable int id_ap, @PathVariable int id_job,
            @PathVariable int status) {
        eduDao.deleteById(id);
        return "redirect:/index/detail/" + id_job + "/" + status + "/edit/" + id_ap;
    }

    @GetMapping("/index/detail/{id_job}/{status}/{id_ap}/delete/workex/{id}")
    public String deleteWorkexs(@PathVariable int id, @PathVariable int id_ap, @PathVariable int id_job,
            @PathVariable int status) {
        workDao.deleteById(id);
        return "redirect:/index/detail/" + id_job + "/" + status + "/edit/" + id_ap;
    }

    @GetMapping("/index/detail/{id_job}/{status}/{id_ap}/delete/train/{id}")
    public String deleteTraining(@PathVariable int id, @PathVariable int id_ap, @PathVariable int id_job,
            @PathVariable int status) {
        trainDao.deleteById(id);
        return "redirect:/index/detail/" + id_job + "/" + status + "/edit/" + id_ap;
    }

    @GetMapping("/index/detail/{id_job}/{status}/{id_ap}/delete/cert/{id}")
    public String deleteCert(@PathVariable int id, @PathVariable int id_ap, @PathVariable int id_job,
            @PathVariable int status) {
        certDao.deleteById(id);
        return "redirect:/index/detail/" + id_job + "/" + status + "/edit/" + id_ap;
    }

    @GetMapping("/index/detail/{id_job}/{status}/{id_ap}/delete/project/{id}")
    public String deleteProject(@PathVariable int id, @PathVariable int id_ap, @PathVariable int id_job,
            @PathVariable int status) {
        projDao.deleteById(id);
        return "redirect:/index/detail/" + id_job + "/" + status + "/edit/" + id_ap;
    }

}