package com.recruitment.batm.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.recruitment.batm.dao.ApplicantsDao;
import com.recruitment.batm.dao.CertificatesDao;
import com.recruitment.batm.dao.EducationsDao;
import com.recruitment.batm.dao.JobDao;
import com.recruitment.batm.dao.ProjectsDao;
import com.recruitment.batm.dao.UserAccessJobDao;
import com.recruitment.batm.dao.UsersDao;
import com.recruitment.batm.dao.WorkexDao;
import com.recruitment.batm.dto.ApplicantsDto;
import com.recruitment.batm.model.Job;
import com.recruitment.batm.model.SpringSession;
import com.recruitment.batm.model.UserAccessJob;
import com.recruitment.batm.dao.SkillsDao;
import com.recruitment.batm.dao.SubMenuDao;
import com.recruitment.batm.dao.TrainingsDao;
import com.recruitment.batm.dao.SpringSessionDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class RecruitmentController {

    @Autowired
    private JobDao jobDao;

    @Autowired
    private UserAccessJobDao userAccessJobDao;

    @Autowired
    private ApplicantsDao applicantsDao;

    @Autowired
    private EducationsDao educationsDao;

    @Autowired
    private WorkexDao workexDao;

    @Autowired
    private SkillsDao skillsDao;

    @Autowired
    private SubMenuDao subMenuDao;

    @Autowired
    SpringSessionDao springSessionDao;

    @Autowired
    UsersDao userDao;

    @Autowired
    private TrainingsDao trainingDao;

    @Autowired
    private CertificatesDao certificateDao;

    @Autowired
    private ProjectsDao projectsDao;


     
    @GetMapping("/index")
    public String viewIndex(Model model, HttpSession session) {
        model.addAttribute("submenu", subMenuDao.findAll());
        
        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        model.addAttribute("userm", userDao.findUsersByUname(namauser));


        model.addAttribute("classActiveSettings","filter-active");
        List<Job> jobjob = jobDao.findJobExceptGDP();
    
        model.addAttribute("listJob", jobjob);
       
        List<Integer> listuj = new ArrayList<Integer>();

        List<Job> b = (List<Job>) jobDao.findAll();     
           
        for (int i = 1; i < b.size()+1 ; i++) {
        listuj.add(userAccessJobDao.jumlahAplicans(i));
        listuj.add(userAccessJobDao.jumlahShortlist(i));
        listuj.add(userAccessJobDao.jumlahInterview(i));
        listuj.add(userAccessJobDao.jumlahOffered(i));
        listuj.add(userAccessJobDao.jumlahHired(i));
        listuj.add(userAccessJobDao.jumlahArchive(i));
        }
        List<List<Integer>> a = new ArrayList<List<Integer>>();
        int c = 0;
        for(int i=0;i<(listuj.size()/6);i++){
            List<Integer> temp = new ArrayList<Integer>();
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            a.add(temp);
        }
        a.remove(0);


        model.addAttribute("listjumlah", a);

        return "index";
    }

    @GetMapping("/index2")
    public String viewIndex2(Model model, HttpSession session){
        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        model.addAttribute("userm", userDao.findUsersByUname(namauser));


        model.addAttribute("classActiveSettings1","filter-active");
        model.addAttribute("listJob", jobDao.findJobById(1));
        
        final int jobid = jobDao.findJobById(1).getId_job();
        
        List<Integer> listuj = new ArrayList<Integer>();
        listuj.add(userAccessJobDao.jumlahAplicans(jobid));
        listuj.add(userAccessJobDao.jumlahShortlist(jobid));
        listuj.add(userAccessJobDao.jumlahInterview(jobid));
        listuj.add(userAccessJobDao.jumlahOffered(jobid));
        listuj.add(userAccessJobDao.jumlahHired(jobid));
        listuj.add(userAccessJobDao.jumlahArchive(jobid));
        
        List<List<Integer>> a = new ArrayList<List<Integer>>();
        int c = 0;
        for(int i=0;i<(listuj.size()/6);i++){
            List<Integer> temp = new ArrayList<Integer>();
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            temp.add(listuj.get(c++));
            a.add(temp);
        }

        

        model.addAttribute("listjumlah", a);

        return "index";
    }

    @GetMapping("/index/detail/{id_job}/{status}")
    public String detail(Model model, @PathVariable int id_job, @PathVariable int status, HttpSession session){
        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        model.addAttribute("userm", userDao.findUsersByUname(namauser));

        model.addAttribute("job", jobDao.findById(id_job).get());
        model.addAttribute("useracc",userAccessJobDao.findByJobStatus(id_job, status));

        List<ApplicantsDto> list = new ArrayList<ApplicantsDto>();
        List<ApplicantsDto> listWE = new ArrayList<ApplicantsDto>();

        List<UserAccessJob> uajList = userAccessJobDao.findByJobnStatus(id_job, status);
        
        List<Integer> listYear = new ArrayList<Integer>();
        List<Integer> listMonth = new ArrayList<Integer>();
        
        for(UserAccessJob i : uajList){
            ApplicantsDto applicantsData = new ApplicantsDto();
            ApplicantsDto applicantsWE = new ApplicantsDto();
            
            applicantsData.setApplicants(applicantsDao.findById(i.getId_ap()).get());
            applicantsData.setEducations(educationsDao.findEducationsByIdAp(i.getId_ap()));
            applicantsData.setWorkexs(workexDao.findWorkexByIdAp(i.getId_ap()));
            applicantsData.setSkills(skillsDao.findApplicanSkills(i.getId_ap()));
            applicantsData.setTrainings(trainingDao.findTrainingById(i.getId_ap()));
            applicantsData.setCertificates(certificateDao.findApplicanCertificates(i.getId_ap()));
            applicantsData.setProjects(projectsDao.findProjectsByIdAp(i.getId_ap()));
            list.add(applicantsData);

            applicantsWE.setWorkexs(workexDao.findTopWorkexByIdAp(i.getId_ap()));
            applicantsWE.setEducations(educationsDao.findTopEducationsByIdAp(i.getId_ap()));
            listWE.add(applicantsWE);

            listYear.add(workexDao.findYearDif(i.getId_ap()));
            listMonth.add(workexDao.findMonthDif(i.getId_ap()));

            
        }

        model.addAttribute("listApplicants", list);
        model.addAttribute("listWE", listWE);
        model.addAttribute("listUaj", uajList);

        model.addAttribute("viewYear", listYear);
        model.addAttribute("viewMonth", listMonth);

        List<Integer> listuj = new ArrayList<Integer>();
        listuj.add(userAccessJobDao.jumlahAplicans(id_job));
        listuj.add(userAccessJobDao.jumlahShortlist(id_job));
        listuj.add(userAccessJobDao.jumlahInterview(id_job));
        listuj.add(userAccessJobDao.jumlahOffered(id_job));
        listuj.add(userAccessJobDao.jumlahHired(id_job));
        listuj.add(userAccessJobDao.jumlahArchive(id_job));
       
        model.addAttribute("listjumlah", listuj);

        return "detail";
    }

    @GetMapping("/index/detail/{id_job}/update/{id_applicants_access_job}/{status}")
    public String updateStatusApplicants2(Model model, @PathVariable int id_job, @PathVariable int id_applicants_access_job, @PathVariable int status){
        UserAccessJob uaj = userAccessJobDao.findById(id_applicants_access_job).get();
        int stat = uaj.getStatus();
        uaj.setStatus(status);        
        model.addAttribute("update", userAccessJobDao.save(uaj));
        return "redirect:/index/detail/"+uaj.getId_job()+"/"+stat;
    }


}
