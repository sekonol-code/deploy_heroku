package com.recruitment.batm.controller;

import javax.servlet.http.HttpSession;

import com.recruitment.batm.dao.SpringSessionDao;
import com.recruitment.batm.dao.SubMenuDao;
import com.recruitment.batm.dao.UsersDao;
import com.recruitment.batm.model.SpringSession;
import com.recruitment.batm.model.Users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController{


    @Autowired 
    SpringSessionDao springSessionDao;

    @Autowired
    private SubMenuDao subMenuDao;

    @Autowired
    UsersDao userDao;
    
    @GetMapping("/login")
    public String login(Model model, HttpSession session){
        model.addAttribute("submenu", subMenuDao.findAll());
        
        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        Users u = userDao.findUsersByUname(namauser);
        model.addAttribute("userm", u);

        try {
            if (u.getId_role()==1) {
                
                return "redirect:/index2";
            
            }
            else {
                return "redirect:/index";
            } 
        } catch (Exception e) {
            return "login";
        }
    }

    @GetMapping("/admin")
    public String viewhome(Model model, HttpSession session) {
        model.addAttribute("submenu", subMenuDao.findAll());
        
        try {
            String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        Users u = userDao.findUsersByUname(namauser);
        model.addAttribute("userm", u);

        try {
            if (u.getId_role()==1) {
                
                return "redirect:/index2";
            
            }
            else {
                return "redirect:/index";
            } 
        } catch (Exception e) {
            return "index";
        }
            
        } catch (Exception e) {
            return "index";
        }
         

        

    }

}
