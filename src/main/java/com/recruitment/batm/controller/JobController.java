package com.recruitment.batm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.recruitment.batm.dao.JobDao;
import com.recruitment.batm.dao.DescriptionDao;
import com.recruitment.batm.dao.QualificationDao;
import com.recruitment.batm.dao.SpringSessionDao;
import com.recruitment.batm.dao.UsersDao;
import com.recruitment.batm.dto.JobDTO;
import com.recruitment.batm.model.Job;
import com.recruitment.batm.model.Description;
import com.recruitment.batm.model.Qualification;
import com.recruitment.batm.model.SpringSession;

@Controller
public class JobController {

    @Autowired
    private JobDao jobDao;

    @Autowired
    private DescriptionDao descriptionDao;

    @Autowired
    private QualificationDao qualificationDao;

    @Autowired
    SpringSessionDao springSessionDao;

    @Autowired
    UsersDao userDao;

    @RequestMapping("/")
    public String showHome(Model model) {
        model.addAttribute("job", jobDao.findAll());
        return "home/Home";
    }

    @GetMapping("index/jobs/create")
    private String showFromPostJob(Model model, HttpSession session) {
        Description desc = new Description();
        Qualification qual = new Qualification();
        List<Description> lDesc = new ArrayList<Description>();
        List<Qualification> lQual = new ArrayList<Qualification>();
        lDesc.add(desc);
        lQual.add(qual);
        JobDTO newJob = new JobDTO();
        newJob.setDescriptionList(lDesc);
        newJob.setQualificationList(lQual);
        model.addAttribute("jobDto", newJob);

        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        model.addAttribute("userm", userDao.findUsersByUname(namauser));

        return "FormJob";
    }

    @PostMapping("/jobs/create")
    private String saveDataPegawai(Model model, @Valid @ModelAttribute("jobDto") JobDTO job_dto,
    BindingResult bindingResult,RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            System.out.println("aaaaa " + bindingResult);
            return "FormJob";
        }
        Job j = job_dto.getJob();
        Job job_dao = jobDao.save(j);
        int id_job = job_dao.getId_job();
        List<Integer> listd = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            listd.add(i);
        }
        model.addAttribute("urutan", listd);
        model.addAttribute("job", jobDao.save(job_dao));

        List<Description> desc_list = job_dto.getDescriptionList();

        model.addAttribute("descriptionList", desc_list);
        for (Description d : desc_list) {
            d.setId_job(id_job);
            descriptionDao.save(d);
        }

        List<Qualification> qual_list = job_dto.getQualificationList();

        model.addAttribute("qualificationList", qual_list);
        for (Qualification q : qual_list) {
            q.setId_job(id_job);
            qualificationDao.save(q);
        }
        redirectAttributes .addFlashAttribute("message", "success");
        return "redirect:/index";
    }

    @RequestMapping("/home/pageProfesional")
    public String pageProfesional(Model model) {
        List<JobDTO> jobList = new ArrayList<JobDTO>();
        List<Job> jobProfessional = jobDao.findJobExceptGDP();
        for (Job j : jobProfessional) {
            JobDTO temp = new JobDTO();
            temp.setJob(j);
            List<Description> descriptions = descriptionDao.findDescJob(j.getId_job());
            temp.setDescriptionList(descriptions);
            List<Qualification> qualifications = qualificationDao.findQualJob(j.getId_job());
            temp.setQualificationList(qualifications);
            jobList.add(temp);
        }

        model.addAttribute("listjob", jobList);
        return "home/pageProfesional";
    }

    @GetMapping("/home/view/{id_job}")
    private String viewPostJobs(@PathVariable Integer id_job, Model model) {
        Optional<Job> j = jobDao.findById(id_job);
        model.addAttribute("job", j.get());
        int id_job_baru = j.get().getId_job();
        model.addAttribute("desc_id_job", descriptionDao.findDescJob(id_job_baru));
        model.addAttribute("qual_id_job", qualificationDao.findQualJob(id_job_baru));

        if (id_job == 1) {
            return "home/pageGDP";
        } else {
            return "home/pageProfesional";
        }
    }

    @GetMapping("index/detail/{id_job}/jobs/views")
    private String views(@PathVariable Integer id_job, Model model) {
        Optional<Job> j = jobDao.findById(id_job);
        model.addAttribute("job", j.get());
        int id_job_baru = j.get().getId_job();
        model.addAttribute("desc_id_job", descriptionDao.findDescJob(id_job_baru));
        model.addAttribute("qual_id_job", qualificationDao.findQualJob(id_job_baru));

        return "viewjob";
    }

    @GetMapping("index/detail/{id_job}/jobs/edit")
    private String edit(@PathVariable Integer id_job, Model model, HttpSession session) {
        JobDTO jobdto = new JobDTO();
        Optional<Job> j = jobDao.findById(id_job);
        jobdto.setJob(j.get());
        jobdto.setDescriptionList(descriptionDao.findDescJob(id_job));
        jobdto.setQualificationList(qualificationDao.findQualJob(id_job));
        model.addAttribute("jobDto", jobdto);

        String ses = session.getId();
        SpringSession sesion = springSessionDao.findsession(ses);
        String namauser = sesion.getPRINCIPAL_NAME();
        model.addAttribute("userm", userDao.findUsersByUname(namauser));

        return "FormJob";
    }

    @GetMapping("index/detail/{id_job}/jobs/deactivate")
    private String deactivate(@PathVariable Integer id_job) {
        Job j = jobDao.findById(id_job).get();
        j.setIs_active(0);
        jobDao.save(j);

        return "redirect:/admin";
    }

    @GetMapping("index/detail/{id_job}/jobs/activate")
    private String activate(Model model, @PathVariable Integer id_job) {
        Job j = jobDao.findById(id_job).get();
        j.setIs_active(1);
        jobDao.save(j);

        return "redirect:/admin";
    }

    @GetMapping("/jobs/{id_job}/delete/desc/{id_desc}")
    public String deleteDesc(@PathVariable Integer id_job, @PathVariable Integer id_desc) {
        descriptionDao.deleteById(id_desc);
        return "redirect:/index/detail/" + id_job + "/jobs/edit";
    }

    @GetMapping("/jobs/{id_job}/delete/qual/{id_qual}")
    public String deleteQual(@PathVariable Integer id_job, @PathVariable Integer id_qual) {
        qualificationDao.deleteById(id_qual);
        return "redirect:/index/detail/" + id_job + "/jobs/edit";
    }

}