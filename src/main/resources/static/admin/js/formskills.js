$("#btnEditSkills").hide();
$("#btnCloseSkills").hide();
$("#btnCloseAddSkills").hide();
$("#valueAllSkills").hide();

$("#btnAddSkills").click(function() {
    $("#btnCloseAddSkills").show();
    $("#btnAddSkills").hide();
});

$("#btnCloseAddSkills").click(function() {
    $("#btnAddSkills").show();
    $("#btnCloseAddSkills").hide();
});

$("#btnEditSkills").click(function() {
    $("#btnCloseSkills").show();
    $("#btnEditSkills").hide();
    $("#valueAllSkills").hide();
});

$("#btnCloseSkills").click(function() {
    $("#btnEditSkills").show();
    $("#btnCloseSkills").hide();
    $("#valueAllSkills").show();
});


// create array for Name Skills
var myArr = [];

// function for save skill and display it
function saveSkills() {

    var yourSkills = document.getElementById("parentLabel").innerText;


    document.getElementById("parentNameSkills").innerHTML = yourSkills;


    
    $("#btnSaveSkills").attr("data-toggle", "collapse");
    $("#btnSaveSkills").attr("data-target", "#collapseOne");
    $("#btnSaveSkills").attr("aria-expanded", "false");
    $("#btnSaveSkills").attr("aria-controls", "collapseOne");
    $("#valueAllSkills").show();
    $("#btnEditSkills").show();
    $("#btnCloseAddSkills").hide();
    $("#btnCloseSkills").hide();
}


// function for push or add skills at form
function pushSkills() {

    // get value skills from the input text
    var inputText = document.getElementById('inputText').value;

    if (inputText === "") {
        // if value is null
        $('#skillsEmptyModal').modal('show');

    } else if (inputText !== null) {
        myArr.push(inputText);
        var uniqueNames = getUnique(myArr);

        if (hasDuplicates(myArr)) {
            // if value skills is same 
            $('#skillsSameModal').modal('show');
            myArr = uniqueNames.slice(0);

        } else if (uniqueNames.length == 1) {
            document.getElementById('parentLabel').innerHTML += `<label id="skill-${uniqueNames.length}" style="margin-top: 30px;font-weight: bold;">${inputText}</label><br>`;
            var x = document.createElement("div");
            x.innerHTML += `<div class="form-group-${uniqueNames.length}">` +
                `<label for="skill-option-${uniqueNames.length}"></label>` +
                // `<select class="form-control" id="skill-option-${uniqueNames.length}">` +
                // `<option selected="selected">Level</option>` +
                // `<option>Beginner</option>` +
                // `<option>Intermediate</option>` +
                // `<option>Advanced</option>` +
                // `<option>Expert</option>` +
                // `</select>` +
                `</div>`;
            document.getElementById('parentOption').append(x);

            // display button save if array length === 1
            document.getElementById('btnSave').innerHTML += `<button id="btnSaveSkills" onclick="saveSkills();" style="padding-left: 20px; padding-right: 20px;" type="button" class="btn btn-success">Save Skills</button>`
        } else {
            document.getElementById('parentLabel').innerHTML += `<label id="skill-${uniqueNames.length}" style="margin-top: 30px;font-weight: bold;">${inputText}</label><br>`;
            var x = document.createElement("div");
            x.innerHTML += `<div class="form-group-${uniqueNames.length}">` +
                `<label for="skill-option-${uniqueNames.length}"></label>` +
                // `<select class="form-control" id="skill-option-${uniqueNames.length}">` +
                // `<option selected="selected">Level</option>` +
                // `<option>Beginner</option>` +
                // `<option>Intermediate</option>` +
                // `<option>Advanced</option>` +
                // `<option>Expert</option>` +
                // `</select>` +
                `</div>`;
            document.getElementById('parentOption').append(x);
        }
    }

}

// function for get duplicate name in array name skills
function hasDuplicates(arr) {
    var counts = [];
    for (var i = 0; i <= arr.length; i++) {
        if (counts[arr[i]] === undefined) {
            counts[arr[i]] = 1;
        } else {
            return true;
        }
    }
    return false;
}

//function for remove duplicate name in array skills
function getUnique(array) {
    var uniqueArray = [];

    // Loop through array values
    for (var value of array) {
        if (uniqueArray.indexOf(value) === -1) {
            uniqueArray.push(value);
        }
    }
    return uniqueArray;
}


////222222222222222
function pushSkills2() {

    // get value skills from the input text
    var inputText = document.getElementById('inputText2').value;

    if (inputText === "") {
        // if value is null
        $('#skillsEmptyModal').modal('show');

    } else if (inputText !== null) {
        myArr.push(inputText);
        var uniqueNames = getUnique(myArr);

        if (hasDuplicates(myArr)) {
            // if value skills is same 
            $('#skillsSameModal').modal('show');
            myArr = uniqueNames.slice(0);

        } else if (uniqueNames.length == 1) {
            document.getElementById('parentLabel2').innerHTML += `<label id="skill-${uniqueNames.length}" style="margin-top: 30px;font-weight: bold;">${inputText}</label><br>`;
            var x = document.createElement("div");
            x.innerHTML += `<div class="form-group-${uniqueNames.length}">` +
                `<label for="skill-option-${uniqueNames.length}"></label>` +
                // `<select class="form-control" id="skill-option-${uniqueNames.length}">` +
                // `<option selected="selected">Level</option>` +
                // `<option>Beginner</option>` +
                // `<option>Intermediate</option>` +
                // `<option>Advanced</option>` +
                // `<option>Expert</option>` +
                // `</select>` +
                `</div>`;
            document.getElementById('parentOption2').append(x);

            // display button save if array length === 1
            document.getElementById('btnSave').innerHTML += `<button id="btnSaveSkills" onclick="saveSkills();" style="padding-left: 20px; padding-right: 20px;" type="button" class="btn btn-success">Save Skills</button>`
        } else {
            document.getElementById('parentLabel2').innerHTML += `<label id="skill-${uniqueNames.length}" style="margin-top: 30px;font-weight: bold;">${inputText}</label><br>`;
            var x = document.createElement("div");
            x.innerHTML += `<div class="form-group-${uniqueNames.length}">` +
                `<label for="skill-option-${uniqueNames.length}"></label>` +
                // `<select class="form-control" id="skill-option-${uniqueNames.length}">` +
                // `<option selected="selected">Level</option>` +
                // `<option>Beginner</option>` +
                // `<option>Intermediate</option>` +
                // `<option>Advanced</option>` +
                // `<option>Expert</option>` +
                // `</select>` +
                `</div>`;
            document.getElementById('parentOption2').append(x);
        }
    }

}

// function for get duplicate name in array name skills
function hasDuplicates(arr) {
    var counts = [];
    for (var i = 0; i <= arr.length; i++) {
        if (counts[arr[i]] === undefined) {
            counts[arr[i]] = 1;
        } else {
            return true;
        }
    }
    return false;
}

//function for remove duplicate name in array skills
function getUnique(array) {
    var uniqueArray = [];

    // Loop through array values
    for (var value of array) {
        if (uniqueArray.indexOf(value) === -1) {
            uniqueArray.push(value);
        }
    }
    return uniqueArray;
}
