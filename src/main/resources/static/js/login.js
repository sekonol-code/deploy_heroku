const inputs = document.querySelectorAll(".input");


function addcl(){
	let parent = this.parentNode.parentNode;
	parent.classList.add("focus");
}

function remcl(){
	let parent = this.parentNode.parentNode;
	if(this.value == ""){
		parent.classList.remove("focus");
	}
}

$(document).on('click', '.toggle-password', function () {

	$(this).toggleClass("fa-eye fa-eye-slash");

	var input = $("#pass_log_id");
	input.attr('type') === 'password' ? input.attr('type', 'text') : input.attr('type', 'password')
});


inputs.forEach(input => {
	input.addEventListener("focus", addcl);
	input.addEventListener("blur", remcl);
});
