$("#btnCloseCertificate").hide();
$("#btnCloseAddCertificate").hide();
$("#icon-check-certificate").hide();
$("#btnEditCertificate").hide();

$("#btnAddCertificate").click(function() {
    $("#btnCloseAddCertificate").show();
    $("#btnAddCertificate").hide();
});

$("#btnCloseAddCertificate").click(function() {
    $("#btnAddCertificate").show();
    $("#btnCloseAddCertificate").hide();
});

$("#btnCloseCertificate").click(function() {
    $("#btnCloseCertificate").hide();
    $("#icon-check-certificate").show();
    $("#btnEditCertificate").show();
});

$("#btnEditCertificate").click(function () {
    $("#btnCloseCertificate").show();
    $("#btnEditCertificate").hide();
    $("#valueAllCertificate").hide();
    $("#btnSaveCertificate").hide();
    $("#icon-check-certificate").hide();
    $("#btnSaveCertificate").attr("data-toggle", "");
    $("#btnSaveCertificate").attr("data-target", "");
    $("#btnSaveCertificate").attr("aria-expanded", "");
    $("#btnSaveCertificate").attr("aria-controls", "");
});


function saveCertificate() {

    for (var i = 0; i < ArrCertificate.length; i++) {

        // get Value all field certificate for Backend
        $(`#certificate-val-${ArrCertificate[i]}`).attr('name', 'certificates[' + i + '].nama_certificate');
        $(`#companyCertificate-val-${ArrCertificate[i]}`).attr('name', 'certificates[' + i + '].company');
        $(`#date-certificate-${ArrCertificate[i]}`).attr('name', 'certificates[' + i + '].date');
        $(`#deskripsi-certificate-${ArrCertificate[i]}`).attr('name', 'certificates[' + i + '].description');

        $("#btnSaveCertificate").attr("data-toggle", "collapse");
        $("#btnSaveCertificate").attr("data-target", "#collapseCertificate");
        $("#btnSaveCertificate").attr("aria-expanded", "false");
        $("#btnSaveCertificate").attr("aria-controls", "collapseCertificate");
        $("#btnEditCertificate").show();
        $("#icon-check-certificate").show();
        $("#btnCloseAddCertificate").hide();
        $("#btnCloseCertificate").hide();

    }
}

//function for remove certificate
function removeCertificate(nameCertificate) {
    $("#btnSaveCertificate").show();
    $("#btnCloseCertificate").hide();
    $('#certificate-id-' + nameCertificate).remove();
    $('#companyCertificate-id-' + nameCertificate).remove();
    $('#deskripsi-id-' + nameCertificate).remove();
    $('#btnRemoveCertificate-id-' + nameCertificate).remove();
    var uniqueNames = getUnique(nameCertificate);

    removeValueArray(uniqueNames, nameCertificate);
    removeValueArray(ArrCertificate, nameCertificate);

    if (uniqueNames.length === 0) {
        $("#btnCloseCertificate").hide();
        $("#btnSaveCertificate").hide();
        $("#btnCloseAddCertificate").show();
    }
    $("#btnCloseCertificate").hide();

}

var ArrCertificate = [];
var certificate = 0;
function pushCertificate() {
    var certificateVal = document.getElementById('certificate').value;
    var companyCertificateVal = document.getElementById('companyCertificate').value;
    var dateCertificateVal = document.getElementById('dateCertificate').value;
    var yearCertificateVal = document.getElementById('yearCertificate').value;
    var deskripsiCertificateVal = document.getElementById('deskripsiCertificate').value;

    let removeSpaceCertificate = certificateVal.replace(" ", "-");
    let removeSpaceCertificate2 = dateCertificateVal.replace(" ", "-");
    let removeSpaceCertificate3 = yearCertificateVal.replace(" ", "-");

    let idCertificate  = removeSpaceCertificate  + removeSpaceCertificate3 + removeSpaceCertificate2;

    if (certificateVal === "" || companyCertificateVal === "" || dateCertificateVal === "" || yearCertificateVal === "") {
        $('#modalCertificate').modal('show');
    } else {
        ArrCertificate.push(idCertificate);
        var uniqueNames = getUnique(ArrCertificate);
        if (hasDuplicates(ArrCertificate)) {
            // if value name school and end date is same 
            $('#SameCertificate').modal('show');
            ArrCertificate = uniqueNames.slice(0);           
        } else if (uniqueNames.length === 1) {
            certificate++;

            // push name certificate
            var nameCertificate = document.createElement("div");
            nameCertificate.setAttribute("id", `certificate-id-${idCertificate}`);
            nameCertificate.innerHTML += `<input id="certificate-val-${idCertificate}" value="${certificateVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCertificate').append(nameCertificate);

            // push name company certificate
            var nameCompanyCertificate = document.createElement("div");
            nameCompanyCertificate.setAttribute("id", `companyCertificate-id-${idCertificate}`);
            nameCompanyCertificate.innerHTML += `<input id="companyCertificate-val-${idCertificate}" value="${companyCertificateVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCompanyCertificate').append(nameCompanyCertificate);

            // push date certificate
            var dateCertificate = document.createElement("div");
            dateCertificate.setAttribute("id", `date-id-${idCertificate}`);
            dateCertificate.innerHTML += `<input id="date-certificate-${idCertificate}" value="${yearCertificateVal + dateCertificate}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDateCertificate').append(dateCertificate);

            // push deskripsi certificate
            var deskripsiCertificate = document.createElement("div");
            deskripsiCertificate.setAttribute("id", `deskripsi-id-${idCertificate}`);
            deskripsiCertificate.innerHTML += `<input id="deskripsi-certificate-${idCertificate}" value="${deskripsiCertificateVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiCertificate').append(deskripsiCertificate);

            //push button remove certificate
            var btnRemoveCertificate= document.createElement("div");
            btnRemoveCertificate.setAttribute("id", `btnRemoveCertificate-id-${idCertificate}`);
            btnRemoveCertificate.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeCertificate('${idCertificate}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveCertificate').append(btnRemoveCertificate);

            document.getElementById('certificate').value = "";
            document.getElementById('companyCertificate').value = "";
            document.getElementById('dateCertificate').value = "";
            document.getElementById('yearCertificate').value = "";
            document.getElementById('deskripsiCertificate').value = "";

            $("#btnCloseAddCertificate").hide();

            if (certificate == 1) {
                // display button save if array length === 1
                document.getElementById('parentBtnSaveCertificate').innerHTML += `<button id="btnSaveCertificate" onclick="saveCertificate();" style="margin-top: 25px;" type="button" class="btn btn-success">Save Certificate</button>`
            } else {
                $("#btnSaveCertificate").show();
            }
        } else{
            // push name certificate
            var nameCertificate = document.createElement("div");
            nameCertificate.setAttribute("id", `certificate-id-${idCertificate}`);
            nameCertificate.innerHTML += `<input id="certificate-val-${idCertificate}" value="${certificateVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCertificate').append(nameCertificate);

            // push name company certificate
            var nameCompanyCertificate = document.createElement("div");
            nameCompanyCertificate.setAttribute("id", `companyCertificate-id-${idCertificate}`);
            nameCompanyCertificate.innerHTML += `<input id="companyCertificate-val-${idCertificate}" value="${companyCertificateVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCompanyCertificate').append(nameCompanyCertificate);

            // push date certificate
            var dateCertificate = document.createElement("div");
            dateCertificate.setAttribute("id", `date-id-${idCertificate}`);
            dateCertificate.innerHTML += `<input id="date-certificate-${idCertificate}" value="${yearCertificateVal + dateCertificate}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDateCertificate').append(dateCertificate);

            // push deskripsi certificate
            var deskripsiCertificate = document.createElement("div");
            deskripsiCertificate.setAttribute("id", `certificate-id-${idCertificate}`);
            deskripsiCertificate.innerHTML += `<input id="deskripsi-certificate-${idCertificate}" value="${deskripsiCertificateVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiCertificate').append(deskripsiCertificate);

            //push button remove certificate
            var btnRemoveCertificate= document.createElement("div");
            btnRemoveCertificate.setAttribute("id", `btnRemoveCertificate-id-${idCertificate}`);
            btnRemoveCertificate.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeCertificate('${idCertificate}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveCertificate').append(btnRemoveCertificate);

            document.getElementById('certificate').value = "";
            document.getElementById('companyCertificate').value = "";
            document.getElementById('dateCertificate').value = "";
            document.getElementById('yearCertificate').value = "";
            document.getElementById('deskripsiCertificate').value = "";

            $("#btnSaveCertificate").show();
            $("#btnCloseCertificate").hide();
        }
    }
}