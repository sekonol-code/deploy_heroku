var listSkills = [
    "ActionScript",
    "Angular JS",
    "AppleScript",
    "Asp",
    "BASIC",
    "Bootstrap",
    "C",
    "C++",
    "C#",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Dart",
    "Django",
    "Erlang",
    "Flutter",
    "Fortran",
    "Golang",
    "Groovy",
    "Haskell",
    "HTML/CSS",
    "Java",
    "JavaScript",
    "Lisp",
    "MySQL",
    "Objective-C",
    "Oracle Server",
    "Perl",
    "PHP",
    "PostgreSQL",
    "Python",
    "Ruby",
    "Scala",
    "Scheme",
    "SQL Server",
    "SQLite",
    "Vue JS",
    "Swift",
    "Kotlin",
    "R",
    "Node JS",
    "Mongo DB",
    "Maria DB",
    "Spring Boot",
    "Spring 5",
    "Swelve Js",
    "Hadoop",
    "Spark",
    "ETL",
    ".Net",
    "Visual Basic",
    "Code Igniter",
    "Laravel",
    "Yii 2",
    "Django",
    "React JS",
    "Git",
    "Delphi",
    "Assembly Language",
    "Matlab",
    "D",
    "Groovy",
    "Rust",
    "Express",
    "Flask",
    "Typescript",
    "Scala",
    "Bash/Shell/Powershell",
    "Dart",
    "Elixir"

];

let addSkills = function() {

    $('.mdb-autocomplete').autocomplete({
        source: listSkills,
        autoFocus: true
    });

    let rowIndex = document.querySelectorAll('.itemSkill').length; //we can add mock class to each movie-row

    $('#skillsList').append(
        '<div class="itemSkill">' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-5 renderSkill">' +
        '<div>' +
        '<input type="text" class="form-control" id="skills' + rowIndex + '.id_skill" name="skills[' + rowIndex + '].id_skill" value="0" placeholder="Name Skill" hidden>' +
        '<input type="text" class="form-control mdb-autocomplete" id="skills' + rowIndex + '.skill" name="skills[' + rowIndex + '].skill" placeholder="Name Skill">' +
        '</div>' +
        '</div>' +
        '<div class="form-group col-md-4 renderSkill">' +
        '<div>' +
        '<select class="custom-select" id="skills' + rowIndex + '.level" name="skills[' + rowIndex + '].level">' +
        '<option value="Beginner">Beginner</option>' +
        '<option value="Intermediate">Intermediate</option>' +
        '<option value="Advanced">Advanced</option>' +
        '<option value="Expert">Expert</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-1">' +
        '<a class="btn linkdelete" style="background-color: transparent;"><i style="color: red;" class="fa fa-close"></i></a>' +
        '</div>' +
        '</div>'
    )

    for (var i = 0; i <= rowIndex; i++) {
        $('.mdb-autocomplete').autocomplete({
            source: listSkills,
            autoFocus: true
        });
    }


    $('#skillsList .linkdelete').click(function() {
        $(this).parent().parent().parent().remove();

        let rowIndex = document.querySelectorAll('.itemSkill').length; //we can add mock class to each movie-row
        console.log(rowIndex);

        if (rowIndex == 0) {
            addSkills();
        }

        let tes = $('#skillsList .renderSkill div').children();
        console.log(tes);

        tes.each(function(i, v) {
            if (i % 4 == 0) {
                v.id = 'skills' + (i / 4) + '.' + 'id';
                v.setAttribute('name', 'skills' + '[' + (i / 4) + '].' + 'id');
            } else if (i % 4 == 1) {
                v.id = 'wew';
                v.setAttribute('name', 'wow');
            } else if (i % 4 == 2) {
                v.id = 'skills' + Math.floor(i / 4) + '.' + 'skill';
                v.setAttribute('name', 'skills' + '[' + Math.floor(i / 4) + '].' + 'skill');
            } else if (i % 4 == 3) {
                v.id = 'skills' + Math.floor(i / 4) + '.' + 'level';
                v.setAttribute('name', 'skills' + '[' + Math.floor(i / 4) + '].' + 'level');
            }
        })
    });
};

$('#skillsList .linkdelete').click(function() {
    $(this).parent().parent().parent().remove();

    let rowIndex = document.querySelectorAll('.itemSkill').length; //we can add mock class to each movie-row
    //console.log(rowIndex);
    if (rowIndex == 0) {
        addSkills();
    }

    let tes = $('#skillsList .renderSkill div').children();
    console.log(tes);

    tes.each(function(i, v) {
        if (i % 4 == 0) {
            v.id = 'skills' + (i / 4) + '.' + 'id';
            v.setAttribute('name', 'skills' + '[' + (i / 4) + '].' + 'id');
        } else if (i % 4 == 1) {
            v.id = 'wew';
            v.setAttribute('name', 'wow');
        } else if (i % 4 == 2) {
            v.id = 'skills' + Math.floor(i / 4) + '.' + 'skill';
            v.setAttribute('name', 'skills' + '[' + Math.floor(i / 4) + '].' + 'skill');
        } else if (i % 4 == 3) {
            v.id = 'skills' + Math.floor(i / 4) + '.' + 'level';
            v.setAttribute('name', 'skills' + '[' + Math.floor(i / 4) + '].' + 'level');
        }
    })
});

$(window).on("load", function() {
    let rowIndex = document.querySelectorAll('.itemSkill').length; //we can add mock class to each movie-row
    if (rowIndex == 0) {
        addSkills();
        $('.mdb-autocomplete').autocomplete({
            source: listSkills,
            autoFocus: true
        });
    }
})
