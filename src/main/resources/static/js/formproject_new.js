let addProject = function() {
    let rowIndex = document.querySelectorAll('.itemProject').length; //we can add mock class to each movie-row

    $('#projectList').append('<div class="itemProject" style="margin-top: 40px;">' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-5 renderProject">' +
        '<label class="font-weight-bold">Project Name<sup>*</sup></label>' +
        `<div>` +
        '<input type="text" class="form-control" id="projects' + rowIndex + '.id_project" name="projects[' + rowIndex + '].id_project" placeholder="Name Training" value="0" hidden>' +
        '<input type="text" class="form-control" id="projects' + rowIndex + '.nama_project" name="projects[' + rowIndex + '].nama_project" placeholder="Name Training">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-5 renderProject">' +
        '<label class="font-weight-bold">Position<sup>*</sup></label>' +
        `<div>` +
        '<input type="text" class="form-control" id="projects' + rowIndex + '.posisi_project" name="projects[' + rowIndex + '].posisi_project" placeholder="Position">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-4 renderProject">' +
        '<label class="font-weight-bold">Start Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="projects' + rowIndex + '.date_awal" name="projects[' + rowIndex + '].date_awal" placeholder="Address">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-4 renderProject">' +
        '<label class="font-weight-bold">End Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="projects' + rowIndex + '.date_akhir" name="projects[' + rowIndex + '].date_akhir" placeholder="Statement">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-2"><a style="color: white; margin-top: 45px;" class="btn linkdelete"><i style="color: red;" class="fa fa-close"></i></a></div>' +
        '</div>' +
        '</div>')

    $('#projectList .linkdelete').click(function() {
        $(this).parent().parent().parent().remove();

        let tes = $('.itemProject .form-row .renderProject div').children();
        console.log(tes);

        tes.each(function(i, v) {
            if (i % 5 == 0) {
                v.id = 'projects' + (i / 5) + '.id_project';
                v.setAttribute('name', 'projects' + '[' + (i / 5) + '].id_project');
            } else if (i % 5 == 1) {
                v.id = 'projects' + Math.floor(i / 5) + '.nama_project';
                v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].nama_project');
            } else if (i % 5 == 2) {
                v.id = 'projects' + Math.floor(i / 5) + '.posisi_project';
                v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].posisi_project');
            } else if (i % 5 == 3) {
                v.id = 'projects' + Math.floor(i / 5) + '.date_awal';
                v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].date_awal');
            } else if (i % 5 == 4) {
                v.id = 'projects' + Math.floor(i / 5) + '.date_akhir';
                v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].date_akhir');
            }
        })

    });
};

$('#projectList .linkdelete').click(function() {
    $(this).parent().parent().parent().remove();

    let tes = $('.itemProject .form-row .renderProject div').children();
    console.log(tes);

    tes.each(function(i, v) {
        if (i % 5 == 0) {
            v.id = 'projects' + (i / 5) + '.id_project';
            v.setAttribute('name', 'projects' + '[' + (i / 5) + '].id_project');
        } else if (i % 5 == 1) {
            v.id = 'projects' + Math.floor(i / 5) + '.nama_project';
            v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].nama_project');
        } else if (i % 5 == 2) {
            v.id = 'projects' + Math.floor(i / 5) + '.posisi_project';
            v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].posisi_project');
        } else if (i % 5 == 3) {
            v.id = 'projects' + Math.floor(i / 5) + '.date_awal';
            v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].date_awal');
        } else if (i % 5 == 4) {
            v.id = 'projects' + Math.floor(i / 5) + '.date_akhir';
            v.setAttribute('name', 'projects' + '[' + Math.floor(i / 5) + '].date_akhir');
        }
    })

});