function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {
        type: mimeString
    });
}

var controlimg = document.getElementById("filejpg");

$(document).ready(function () {

    $image_crop = $('#image_demo').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'square' //circle
        },
        boundary: {
            width: 300,
            height: 300
        }
    });

    $('#filejpg').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function () {
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('.crop_image').click(function (event) {
        $image_crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (imageCrop) {
            var files = controlimg.files;
            $('#uploaded_image').attr('src', imageCrop);
            console.log("Filename crop: " + files[0].name);

            var blob = dataURItoBlob(imageCrop);
            console.log(blob);
            const dt = new DataTransfer();
            dt.items.add(new File([blob], "new.jpg"));
            document.getElementById("filejpg").files = dt.files;

            //console.log(files[0].name);
            $('#uploadimageModal').modal('hide');
        });
    });
});

controlimg.addEventListener("change", function (event) {
    // When the control has changed, there are new files
    var files = controlimg.files;

    if (files[0] != null) {
        var img = new Image();
        console.log("Filename: " + files[0].name);
        console.log("Type: " + files[0].type);
        console.log("Size: " + files[0].size + " bytes");
        if (files[0].size > 1000000) {
            //File too big
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'File too Big!',
            })
            controlimg.value = null;
            document.getElementById("registerButton").disabled = true;
        } else {
            var fileReader = new FileReader();
            var type;
            fileReader.onloadend = function (e) {
                var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                var header = "";
                for (var i = 0; i < arr.length; i++) {
                    header += arr[i].toString(16);
                }
                console.log(header);

                // Check the file signature against known types
                switch (header) {
                    case "89504e47":
                    case "ffd8ffe0":
                    case "ffd8ffe1":
                    case "ffd8ffe2":
                    case "ffd8ffe3":
                    case "ffd8ffe8":
                        img.onload = function () {
                            var sizes = {
                                width: this.width,
                                height: this.height
                            };
                            URL.revokeObjectURL(this.src);

                            console.log('onload: sizes', sizes);
                            console.log('onload: this', this);
                        }

                        var objectURL = URL.createObjectURL(files[0]);

                        console.log('change: file', files[0]);
                        console.log('change: objectURL', objectURL);
                        img.src = objectURL;

                        if (img.sizes.height < 400 || img.sizes.width < 300) {
                            //Files dimension too small
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Dimension image too small!',
                            })
                            console.log("disabled");
                            controlimg.value = null;
                            document.getElementById("registerButton").disabled = true; // Or you can use the blob.type as fallback
                        } else {
                            console.log("enabled");
                            $('#uploadimageModal').modal('show');
                            document.getElementById("registerButton").disabled = false; // Or you can use the blob.type as fallback
                        }
                        break;
                    default:
                        //Files is not jpg/jpeg/png
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Format image not Accepted!',
                        })
                        console.log("disabled");
                        document.getElementById("registerButton").disabled = true; // Or you can use the blob.type as fallback
                        break;
                }

            };
            fileReader.readAsArrayBuffer(files[0]);

            console.log(files[0].type);
        }

    } else {
        document.getElementById("registerButton").disabled = false;
    }

}, false);