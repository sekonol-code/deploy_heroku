$("#btnCloseTraining").hide();
$("#btnCloseAddTraining").hide();
$("#icon-check-training").hide();
$("#btnEditTraining").hide();

$("#btnAddTraining").click(function() {
    $("#btnCloseAddTraining").show();
    $("#btnAddTraining").hide();
});

$("#btnCloseAddTraining").click(function() {
    $("#btnAddTraining").show();
    $("#btnCloseAddTraining").hide();
});

$("#btnCloseTraining").click(function() {
    $("#btnCloseTraining").hide();
    $("#icon-check-training").show();
    $("#btnEditTraining").show();
});

$("#btnEditTraining").click(function () {
    $("#btnCloseTraining").show();
    $("#btnEditTraining").hide();
    $("#valueAllTraining").hide();
    $("#btnSaveTraining").hide();
    $("#icon-check-training").hide();
    $("#btnSaveTraining").attr("data-toggle", "");
    $("#btnSaveTraining").attr("data-target", "");
    $("#btnSaveTraining").attr("aria-expanded", "");
    $("#btnSaveTraining").attr("aria-controls", "");
});

function saveTraining() {

    for (var i = 0; i < ArrTraining.length; i++) {

        // get Value all field training for Backend
        $(`#training-val-${ArrTraining[i]}`).attr('name', 'trainings[' + i + '].ntraining');
        $(`#companyTraining-val-${ArrTraining[i]}`).attr('name', 'trainings[' + i + '].company');
        $(`#begin-training-${ArrTraining[i]}`).attr('name', 'trainings[' + i + '].date_awal');
        $(`#end-training-${ArrTraining[i]}`).attr('name', 'trainings[' + i + '].date_akhir');
        $(`#deskripsi-training-${ArrTraining[i]}`).attr('name', 'trainings[' + i + '].description');

        $("#btnSaveTraining").attr("data-toggle", "collapse");
        $("#btnSaveTraining").attr("data-target", "#collapseTraining");
        $("#btnSaveTraining").attr("aria-expanded", "false");
        $("#btnSaveTraining").attr("aria-controls", "collapseTraining");
        $("#btnEditTraining").show();
        $("#icon-check-training").show();
        $("#btnCloseAddTraining").hide();
        $("#btnCloseTraining").hide();

    }
}

//function for remove training
function removeTraining(nameTraining) {
    $("#btnSaveTraining").show();
    $("#btnCloseTraining").hide();
    $('#training-id-' + nameTraining).remove();
    $('#companyTraining-id-' + nameTraining).remove();
    $('#begin-id-' + nameTraining).remove();
    $('#end-id-' + nameTraining).remove();
    $('#deskripsi-id-' + nameTraining).remove();
    $('#btnRemoveTraining-id-' + nameTraining).remove();
    var uniqueNames = getUnique(ArrTraining);

    removeValueArray(uniqueNames, nameTraining);
    removeValueArray(ArrTraining, nameTraining);

    if (uniqueNames.length === 0) {
        $("#btnCloseTraining").hide();
        $("#btnSaveTraining").hide();
        $("#btnCloseAddTraining").show();
    }
    $("#btnCloseTraining").hide();

}

var ArrTraining = [];
var training = 0;
function pushTraining() {
    let trainingVal = document.getElementById('training').value;
    let companyTrainingVal = document.getElementById('companyTraining').value;
    let dateawalTrainingVal = document.getElementById('dateawalTraining').value;
    let yearawalTrainingVal = document.getElementById('yearawalTraining').value;
    let dateakhirTrainingVal = document.getElementById('dateakhirTraining').value;
    let yearakhirTrainingVal = document.getElementById('yearakhirTraining').value;
    let deskripsiTrainingVal = document.getElementById('deskripsiTraining').value;

    let removeSpaceTraining = trainingVal.replace(" ", "-");
    let removeSpaceTraining2 = dateakhirTrainingVal.replace(" ", "-");
    let removeSpaceTraining3 = yearakhirTrainingVal.replace(" ", "-");

    let idTraining = removeSpaceTraining + removeSpaceTraining3 + removeSpaceTraining2;

    if (trainingVal === "" || companyTrainingVal === "" || dateawalTrainingVal === "" ||
        yearawalTrainingVal === "" || dateakhirTrainingVal === "" || yearakhirTrainingVal === "") {
        $('#modalTraining').modal('show');
    } else {
        ArrTraining.push(idTraining);
        var uniqueNames = getUnique(ArrTraining);
        if (hasDuplicates(ArrTraining)) {
            // if value name school and end date is same 
            $('#SameTraining').modal('show');
            ArrTraining = uniqueNames.slice(0);           
        } else if (uniqueNames.length === 1) {
            training++;

            // push name training
            var nameTraining = document.createElement("div");
            nameTraining.setAttribute("id", `training-id-${idTraining}`);
            nameTraining.innerHTML += `<input id="training-val-${idTraining}" value="${trainingVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameTraining').append(nameTraining);

            // push name company training
            var nameCompanyTraining = document.createElement("div");
            nameCompanyTraining.setAttribute("id", `companyTraining-id-${idTraining}`);
            nameCompanyTraining.innerHTML += `<input id="companyTraining-val-${idTraining}" value="${companyTrainingVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCompanyTraining').append(nameCompanyTraining);

            // push date begin training
            var beginTraining = document.createElement("div");
            beginTraining.setAttribute("id", `begin-id-${idTraining}`);
            beginTraining.innerHTML += `<input id="begin-training-${idTraining}" value="${yearawalTrainingVal + dateawalTrainingVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentBeginTraining').append(beginTraining);

            // push date end training
            var endTraining= document.createElement("div");
            endTraining.setAttribute("id", `end-id-${idTraining}`);
            endTraining.innerHTML += `<input id="end-training-${idTraining}" value="${yearakhirTrainingVal + dateakhirTrainingVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentEndTraining').append(endTraining);

            // push deskripsi training
            var deskripsiTraining = document.createElement("div");
            deskripsiTraining.setAttribute("id", `deskripsi-id-${idTraining}`);
            deskripsiTraining.innerHTML += `<input id="deskripsi-training-${idTraining}" value="${deskripsiTrainingVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiTraining').append(deskripsiTraining);

            //push button remove training
            var btnRemoveTraining= document.createElement("div");
            btnRemoveTraining.setAttribute("id", `btnRemoveTraining-id-${idTraining}`);
            btnRemoveTraining.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeTraining('${idTraining}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveTraining').append(btnRemoveTraining);

            document.getElementById('training').value = "";
            document.getElementById('companyTraining').value = "";
            document.getElementById('dateawalTraining').value = "";
            document.getElementById('yearawalTraining').value = "";
            document.getElementById('dateakhirTraining').value = "";
            document.getElementById('yearakhirTraining').value = "";
            document.getElementById('deskripsiTraining').value = "";

            $("#btnCloseAddTraining").hide();

            if (training == 1) {
                // display button save if array length === 1
                document.getElementById('parentBtnSaveTraining').innerHTML += `<button id="btnSaveTraining" onclick="saveTraining();" style="margin-top: 25px;" type="button" class="btn btn-success">Save Training</button>`
            } else {
                $("#btnSaveTraining").show();
            }
        } else {
            // push name training
            var nameTraining = document.createElement("div");
            nameTraining.setAttribute("id", `training-id-${idTraining}`);
            nameTraining.innerHTML += `<input id="training-val-${idTraining}" value="${trainingVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameTraining').append(nameTraining);

            // push name company training
            var nameCompanyTraining = document.createElement("div");
            nameCompanyTraining.setAttribute("id", `companyTraining-id-${idTraining}`);
            nameCompanyTraining.innerHTML += `<input id="companyTraining-val-${idTraining}" value="${companyTrainingVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCompanyTraining').append(nameCompanyTraining);

            // push date begin training
            var beginTraining = document.createElement("div");
            beginTraining.setAttribute("id", `begin-id-${idTraining}`);
            beginTraining.innerHTML += `<input id="begin-training-${idTraining}" value="${yearawalTrainingVal + dateawalTrainingVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentBeginTraining').append(beginTraining);

            // push date end training
            var endTraining= document.createElement("div");
            endTraining.setAttribute("id", `end-id-${idTraining}`);
            endTraining.innerHTML += `<input id="end-training-${idTraining}" value="${yearakhirTrainingVal + dateakhirTrainingVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentEndTraining').append(endTraining);

            // push deskripsi training
            var deskripsiTraining = document.createElement("div");
            deskripsiTraining.setAttribute("id", `deskripsi-id-${idTraining}`);
            deskripsiTraining.innerHTML += `<input id="deskripsi-training-${idTraining}" value="${deskripsiTrainingVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiTraining').append(deskripsiTraining);

            //push button remove training
            var btnRemoveTraining= document.createElement("div");
            btnRemoveTraining.setAttribute("id", `btnRemoveTraining-id-${idTraining}`);
            btnRemoveTraining.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeTraining('${idTraining}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveTraining').append(btnRemoveTraining);

            document.getElementById('training').value = "";
            document.getElementById('companyTraining').value = "";
            document.getElementById('dateawalTraining').value = "";
            document.getElementById('yearawalTraining').value = "";
            document.getElementById('dateakhirTraining').value = "";
            document.getElementById('yearakhirTraining').value = "";
            document.getElementById('deskripsiTraining').value = "";

            $("#btnSaveTraining").show();
            $("#btnCloseTraining").hide();
        }
    }
}