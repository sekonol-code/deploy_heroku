$("#btnCloseEducation").hide();
$("#btnCloseAddEducation").hide();
$("#icon-check-education").hide();
$("#btnEditEducation").hide();


$("#btnAddEducation").click(function () {
    $("#btnCloseAddEducation").show();
    $("#btnAddEducation").hide();
});

$("#btnCloseAddEducation").click(function () {
    $("#btnAddEducation").show();
    $("#btnCloseAddEducation").hide();
});

$("#btnCloseEducation").click(function () {
    $("#btnCloseEducation").hide();
    $("#icon-check-education").show();
    $("#btnEditEducation").show();
});

$("#btnEditEducation").click(function () {
    $("#btnCloseEducation").show();
    $("#btnEditEducation").hide();
    $("#valueAllEducation").hide();
    $("#btnSaveEducation").hide();
    $("#icon-check-education").hide();
    $("#btnSaveEducation").attr("data-toggle", "");
    $("#btnSaveEducation").attr("data-target", "");
    $("#btnSaveEducation").attr("aria-expanded", "");
    $("#btnSaveEducation").attr("aria-controls", "");
});

function saveEducation() {

    for (var i = 0; i < ArrEducation.length; i++) {

        // get Value all field education for Backend
        $(`#school-val-${ArrEducation[i]}`).attr('name', 'education[' + i + '].school');
        $(`#major-val-${ArrEducation[i]}`).attr('name', 'education[' + i + '].major');
        $(`#begin-education-${ArrEducation[i]}`).attr('name', 'education[' + i + '].date_awal');
        $(`#end-education-${ArrEducation[i]}`).attr('name', 'education[' + i + '].date_akhir');
        $(`#deskripsi-education-${ArrEducation[i]}`).attr('name', 'education[' + i + '].description');

        $("#btnSaveEducation").attr("data-toggle", "collapse");
        $("#btnSaveEducation").attr("data-target", "#collapseEducation");
        $("#btnSaveEducation").attr("aria-expanded", "false");
        $("#btnSaveEducation").attr("aria-controls", "collapseEducation");
        $("#btnEditEducation").show();
        $("#icon-check-education").show();
        $("#btnCloseAddEducation").hide();
        $("#btnCloseEducation").hide();

    }
}

//function for remove education
function removeEducation(nameEducation) {
    $("#btnSaveEducation").show();
    $("#btnCloseEducation").hide();
    $('#school-id-' + nameEducation).remove();
    $('#major-id-' + nameEducation).remove();
    $('#begin-id-' + nameEducation).remove();
    $('#end-id-' + nameEducation).remove();
    $('#deskripsi-id-' + nameEducation).remove();
    $('#btnRemoveEducation-id-' + nameEducation).remove();
    var uniqueNames = getUnique(ArrEducation);

    removeValueArray(uniqueNames, nameEducation);
    removeValueArray(ArrEducation, nameEducation);

    if (uniqueNames.length === 0) {
        $("#btnCloseEducation").hide();
        $("#btnSaveEducation").hide();
        $("#btnCloseAddEducation").show();
    }
    // console.log(uniqueNames);
    // console.log(myArr);
    $("#btnCloseEducation").hide();

}


var ArrEducation = [];
var education = 0;

function pushEducation() {
    let schoolVal = document.getElementById('school').value;
    let majorVal = document.getElementById('major').value;
    let dateawalEducationVal = document.getElementById('dateawalEducation').value;
    let yearawalEducationVal = document.getElementById('yearawalEducation').value;
    let dateakhirEducationVal = document.getElementById('dateakhirEducation').value;
    let yearakhirEducationVal = document.getElementById('yearakhirEducation').value;
    let deskripsiEducationVal = document.getElementById('deskripsiEducation').value;

    let removeSpaceEducation = schoolVal.replace(" ", "-");
    let removeSpaceEducation2 = dateakhirEducationVal.replace(" ", "-");
    let removeSpaceEducation3 = yearakhirEducationVal.replace(" ", "-");

    let idEducation = removeSpaceEducation + removeSpaceEducation3 + removeSpaceEducation2;

    if (schoolVal === "" || majorVal === "" || dateawalEducationVal === "" ||
        yearawalEducationVal === "" || dateakhirEducationVal === "" || yearakhirEducationVal === "") {
        $('#modalEducation').modal('show');
    } else {
        ArrEducation.push(idEducation);
        var uniqueNames = getUnique(ArrEducation);
        if (hasDuplicates(ArrEducation)) {
            // if value name school and end date is same 
            $('#SameEducation').modal('show');
            ArrEducation = uniqueNames.slice(0);
        } else if (uniqueNames.length === 1) {
            education++;

            // push name school
            var nameSchool = document.createElement("div");
            nameSchool.setAttribute("id", `school-id-${idEducation}`);
            nameSchool.innerHTML += `<input id="school-val-${idEducation}" value="${schoolVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameSchool').append(nameSchool);

            // push name major
            var nameMajor = document.createElement("div");
            nameMajor.setAttribute("id", `major-id-${idEducation}`);
            nameMajor.innerHTML += `<input id="major-val-${idEducation}" value="${majorVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameMajor').append(nameMajor);

            // push date begin education
            var beginEducation = document.createElement("div");
            beginEducation.setAttribute("id", `begin-id-${idEducation}`);
            beginEducation.innerHTML += `<input id="begin-education-${idEducation}" value="${yearawalEducationVal + dateawalEducationVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentBeginEducation').append(beginEducation);

            // push date end education
            var endEducation = document.createElement("div");
            endEducation.setAttribute("id", `end-id-${idEducation}`);
            endEducation.innerHTML += `<input id="end-education-${idEducation}" value="${yearakhirEducationVal + dateakhirEducationVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentEndEducation').append(endEducation);

            // push deskripsi education
            var deskripsiEducation = document.createElement("div");
            deskripsiEducation.setAttribute("id", `deskripsi-id-${idEducation}`);
            deskripsiEducation.innerHTML += `<input id="deskripsi-education-${idEducation}" value="${deskripsiEducationVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiEducation').append(deskripsiEducation);

            //push button remove education
            var btnRemoveEducation = document.createElement("div");
            btnRemoveEducation.setAttribute("id", `btnRemoveEducation-id-${idEducation}`);
            btnRemoveEducation.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeEducation('${idEducation}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveEducation').append(btnRemoveEducation);

            document.getElementById('school').value = "";
            document.getElementById('major').value = "";
            document.getElementById('dateawalEducation').value = "";
            document.getElementById('yearawalEducation').value = "";
            document.getElementById('dateakhirEducation').value = "";
            document.getElementById('yearakhirEducation').value = "";
            document.getElementById('deskripsiEducation').value = "";

            $("#btnCloseAddEducation").hide();

            if (education == 1) {
                // display button save if array length === 1
                document.getElementById('parentBtnSaveEducation').innerHTML += `<button id="btnSaveEducation" onclick="saveEducation();" style="margin-top: 25px;" type="button" class="btn btn-success">Save Education</button>`
            } else {
                $("#btnSaveEducation").show();
            }
        } else {
            // push name school
            var nameSchool = document.createElement("div");
            nameSchool.setAttribute("id", `school-id-${idEducation}`);
            nameSchool.innerHTML += `<input id="school-val-${idEducation}" value="${schoolVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameSchool').append(nameSchool);

            // push name major
            var nameMajor = document.createElement("div");
            nameMajor.setAttribute("id", `major-id-${idEducation}`);
            nameMajor.innerHTML += `<input id="major-val-${idEducation}" value="${majorVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameMajor').append(nameMajor);

            // push date begin education
            var beginEducation = document.createElement("div");
            beginEducation.setAttribute("id", `begin-id-${idEducation}`);
            beginEducation.innerHTML += `<input id="begin-education-${idEducation}" value="${yearawalEducationVal + dateawalEducationVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentBeginEducation').append(beginEducation);

            // push date end education
            var endEducation = document.createElement("div");
            endEducation.setAttribute("id", `end-id-${idEducation}`);
            endEducation.innerHTML += `<input id="end-education-${idEducation}" value="${yearakhirEducationVal + dateakhirEducationVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentEndEducation').append(endEducation);

            // push deskripsi education
            var deskripsiEducation = document.createElement("div");
            deskripsiEducation.setAttribute("id", `deskripsi-id-${idEducation}`);
            deskripsiEducation.innerHTML += `<input id="deskripsi-education-${idEducation}" value="${deskripsiEducationVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiEducation').append(deskripsiEducation);

            //push button remove education
            var btnRemoveEducation = document.createElement("div");
            btnRemoveEducation.setAttribute("id", `btnRemoveEducation-id-${idEducation}`);
            btnRemoveEducation.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeEducation('${idEducation}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveEducation').append(btnRemoveEducation);

            document.getElementById('school').value = "";
            document.getElementById('major').value = "";
            document.getElementById('dateawalEducation').value = "";
            document.getElementById('yearawalEducation').value = "";
            document.getElementById('dateakhirEducation').value = "";
            document.getElementById('yearakhirEducation').value = "";
            document.getElementById('deskripsiEducation').value = "";

            $("#btnSaveEducation").show();
            $("#btnCloseEducation").hide();
        }
    }
}