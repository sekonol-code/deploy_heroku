$("#btnEditSkills").hide();
$("#btnCloseSkills").hide();
$("#btnCloseAddSkills").hide();
$("#valueAllSkills").hide();
$("#icon-check-skills").hide();

$("#btnAddSkills").click(function() {
    $("#btnCloseAddSkills").show();
    $("#btnAddSkills").hide();
});

$("#btnCloseAddSkills").click(function() {
    $("#btnAddSkills").show();
    $("#btnCloseAddSkills").hide();
});

$("#btnEditSkills").click(function() {
    $("#btnCloseSkills").show();
    $("#btnEditSkills").hide();
    $("#valueAllSkills").hide();
    $("#icon-check-skills").hide();
    $("#btnSaveSkills").attr("data-toggle", "");
    $("#btnSaveSkills").attr("data-target", "");
    $("#btnSaveSkills").attr("aria-expanded", "");
    $("#btnSaveSkills").attr("aria-controls", "");
});

$("#btnCloseSkills").click(function() {
    $("#btnEditSkills").show();
    $("#btnCloseSkills").hide();
    $("#valueAllSkills").show();
    $("#icon-check-skills").show();
});


//function for remove skill
function removeSkill(nameSkill) {
    $("#btnCloseSkills").hide();
    $('#skill-id-' + nameSkill).remove();
    $('#skill-option-id-' + nameSkill).remove();
    $('#btnRemoveSkill-id-' + nameSkill).remove();
    var uniqueNames = getUnique(myArr);

    removeValueArray(uniqueNames, nameSkill);
    removeValueArray(myArr, nameSkill);

    if (uniqueNames.length === 0) {
        $("#btnSave").hide();
        $("#btnCloseAddSkills").hide();
    }
    // console.log(uniqueNames);
    // console.log(myArr);
    $("#btnCloseSkills").hide();

}

function removeValueArray(arr) {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


// create array for Name Skills
var myArr = [];
var i = 0;
// function for save skill and display it
function saveSkills() {

    //var yourSkills = document.getElementById("parentLabel").innerText;
    var levelSkillArr = [];

    for (var i = 0; i < myArr.length; i++) {
        var valueLevel = $(`#level-${myArr[i]} option:selected`).text();
        levelSkillArr.push(valueLevel);
        console.log(valueLevel);

        if (valueLevel === "Level") {
            $("#btnSaveSkills").attr("data-toggle", "");
            $("#btnSaveSkills").attr("data-target", "");
            $("#btnSaveSkills").attr("aria-expanded", "");
            $("#btnSaveSkills").attr("aria-controls", "");
            $('#skillsNotChoose').modal('show');
            break;
        } else if (i === myArr.length - 1) {
            //document.getElementById("parentNameSkills").innerHTML = yourSkills;
            $("#btnSaveSkills").attr("data-toggle", "collapse");
            $("#btnSaveSkills").attr("data-target", "#collapseOne");
            $("#btnSaveSkills").attr("aria-expanded", "false");
            $("#btnSaveSkills").attr("aria-controls", "collapseOne");
            //     $("#valueAllSkills").show();
            $("#btnEditSkills").show();
            $("#btnCloseAddSkills").hide();
            $("#btnCloseSkills").hide();
            $("#icon-check-skills").show();
            //     console.log(levelSkillArr);
            //     console.log(myArr);
        }
        // get Value name skill and level for Backend
        $(`#${myArr[i]}`).attr('name', 'skills[' + i + '].skill');
        $(`#${myArr[i]}`).attr('id', 'skills'+ i +'.skill');
        $(`#level-${myArr[i]}`).attr('name', 'skills[' + i + '].level');
        $(`#level-${myArr[i]}`).attr('id', 'skills' + i + '.level');

        $("#btnSaveSkills").attr("data-toggle", "collapse");
        $("#btnSaveSkills").attr("data-target", "#collapseOne");
        $("#btnSaveSkills").attr("aria-expanded", "false");
        $("#btnSaveSkills").attr("aria-controls", "collapseOne");
        $("#valueAllSkills").show();
        $("#btnEditSkills").show();
        $("#btnCloseAddSkills").hide();
        $("#btnCloseSkills").hide();
    }
}

// function for push or add skills at form
function pushSkills() {

    // get value skills from the input text
    let inputText = document.getElementById('inputText').value;
    let idinputText = inputText.replace(" ", "-");

    if (inputText === "") {
        // if value is null
        $('#skillsEmptyModal').modal('show');

    } else if (inputText !== null) {
        myArr.push(idinputText);
        var uniqueNames = getUnique(myArr);

        if (hasDuplicates(myArr)) {
            // if value skills is same 
            $('#skillsSameModal').modal('show');
            myArr = uniqueNames.slice(0);

        } else if (uniqueNames.length == 1) {
            i++;

            // push name skill
            var nameSkill = document.createElement("div");
            nameSkill.setAttribute("id", `skill-id-${idinputText}`);
            nameSkill.innerHTML += `<input id="${idinputText}" value="${inputText}" class="form-control" style="margin-top: 20px;font-weight: bold;" disabled>`;
            document.getElementById('parentLabel').append(nameSkill);

            //push option skill
            var option = document.createElement("div");
            option.setAttribute("id", `skill-option-id-${idinputText}`);
            option.innerHTML += `<div class="form-group-${inputText}">` +
                `<label for="skill-option-${idinputText}"></label>` +
                `<select class="form-control" id="level-${idinputText}">` +
                `<option selected="selected" hidden>Level</option>` +
                `<option>Beginner</option>` +
                `<option>Intermediate</option>` +
                `<option>Advanced</option>` +
                `<option>Expert</option>` +
                `</select>` +
                `</div>`;
            document.getElementById('parentOption').append(option);

            //push button remove skill
            var btnRemoveSkill = document.createElement("div");
            btnRemoveSkill.setAttribute("id", `btnRemoveSkill-id-${idinputText}`);
            btnRemoveSkill.innerHTML += `<button style="margin-top: 20px; background-color: transparent;" type="button" class="btn" onclick="removeSkill('${idinputText}')"><i class="fa fa-close"></i></button><br>`
            document.getElementById('parentBtnRemove').append(btnRemoveSkill);

            if (i == 1) {
                // display button save if array length === 1
                document.getElementById('btnSave').innerHTML += `<button id="btnSaveSkills" onclick="saveSkills();" style="padding-left: 20px; padding-right: 20px;" type="button" class="btn btn-success">Save Skills</button>`
            } else {
                $("#btnSave").show();
            }

        } else {
            // push name skill
            var nameSkill = document.createElement("div");
            nameSkill.setAttribute("id", `skill-id-${idinputText}`);
            nameSkill.innerHTML += `<input id="${idinputText}" value="${inputText}" class="form-control" style="margin-top: 18px;font-weight: bold;" disabled>`;
            document.getElementById('parentLabel').append(nameSkill);

            // push option skill
            var option = document.createElement("div");
            option.setAttribute("id", `skill-option-id-${idinputText}`);
            option.innerHTML += `<div class="form-group-${idinputText}">` +
                `<label for="skill-option-${idinputText}"></label>` +
                `<select class="form-control" id="level-${idinputText}">` +
                `<option selected="selected" hidden>Level</option>` +
                `<option>Beginner</option>` +
                `<option>Intermediate</option>` +
                `<option>Advanced</option>` +
                `<option>Expert</option>` +
                `</select>` +
                `</div>`;
            document.getElementById('parentOption').append(option);

            //push button remove skill
            var btnRemoveSkill = document.createElement("div");
            btnRemoveSkill.setAttribute("id", `btnRemoveSkill-id-${idinputText}`);
            btnRemoveSkill.innerHTML += `<button style="margin-top: 20px; background-color: transparent;" type="button" class="btn" onclick="removeSkill('${idinputText}')"><i class="fa fa-close"></i></button><br>`
            document.getElementById('parentBtnRemove').append(btnRemoveSkill);
        }
    }

}

// function for get duplicate name in array name skills
function hasDuplicates(arr) {
    var counts = [];
    for (var i = 0; i <= arr.length; i++) {
        if (counts[arr[i]] === undefined) {
            counts[arr[i]] = 1;
        } else {
            return true;
        }
    }
    return false;
}

//function for remove duplicate name in array skills
function getUnique(array) {
    var uniqueArray = [];

    // Loop through array values
    for (var value of array) {
        if (uniqueArray.indexOf(value) === -1) {
            uniqueArray.push(value);
        }
    }
    return uniqueArray;
}