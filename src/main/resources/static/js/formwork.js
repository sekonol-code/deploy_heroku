$("#btnCloseWork").hide();
$("#btnCloseAddWork").hide();
$("#icon-check-work").hide();
$("#btnEditWork").hide();

$("#btnAddWork").click(function() {
    $("#btnCloseAddWork").show();
    $("#btnAddWork").hide();
});

$("#btnCloseAddWork").click(function() {
    $("#btnAddWork").show();
    $("#btnCloseAddWork").hide();
});

$("#btnCloseWork").click(function() {
    $("#btnCloseWork").hide();
    $("#icon-check-work").show();
    $("#btnEditWork").show();
});

$("#btnEditWork").click(function () {
    $("#btnCloseWork").show();
    $("#btnEditWork").hide();
    $("#valueAllWork").hide();
    $("#btnSaveWork").hide();
    $("#icon-check-work").hide();
    $("#btnSaveWork").attr("data-toggle", "");
    $("#btnSaveWork").attr("data-target", "");
    $("#btnSaveWork").attr("aria-expanded", "");
    $("#btnSaveWork").attr("aria-controls", "");
});

function saveWork() {

    for (var i = 0; i < ArrWork.length; i++) {

        // get Value all field work for Backend
        $(`#job-val-${ArrWork[i]}`).attr('name', 'workexs[' + i + '].job');
        $(`#company-val-${ArrWork[i]}`).attr('name', 'workexs[' + i + '].company');
        $(`#begin-work-${ArrWork[i]}`).attr('name', 'workexs[' + i + '].date_awal');
        $(`#end-work-${ArrWork[i]}`).attr('name', 'workexs[' + i + '].date_akhir');
        $(`#deskripsi-work-${ArrWork[i]}`).attr('name', 'workexs[' + i + '].description');

        $("#btnSaveWork").attr("data-toggle", "collapse");
        $("#btnSaveWork").attr("data-target", "#collapseWork");
        $("#btnSaveWork").attr("aria-expanded", "false");
        $("#btnSaveWork").attr("aria-controls", "collapseWork");
        $("#btnEditWork").show();
        $("#icon-check-work").show();
        $("#btnCloseAddWork").hide();
        $("#btnCloseWork").hide();

    }
}

//function for remove work
function removeWork(nameWork) {
    $("#btnSaveWork").show();
    $("#btnCloseWork").hide();
    $('#job-id-' + nameWork).remove();
    $('#company-id-' + nameWork).remove();
    $('#begin-id-' + nameWork).remove();
    $('#end-id-' + nameWork).remove();
    $('#deskripsi-id-' + nameWork).remove();
    $('#btnRemoveWork-id-' + nameWork).remove();
    var uniqueNames = getUnique(ArrWork);

    removeValueArray(uniqueNames, nameWork);
    removeValueArray(ArrWork, nameWork);

    if (uniqueNames.length === 0) {
        $("#btnCloseWork").hide();
        $("#btnSaveWork").hide();
        $("#btnCloseAddWork").show();
    }
    $("#btnCloseWork").hide();

}

var ArrWork = [];
var work = 0;
function pushWork() {
    let jobVal = document.getElementById('job').value;
    let companyVal = document.getElementById('company').value;
    let dateawalWorkVal = document.getElementById('dateawalWork').value;
    let yearawalWorkVal = document.getElementById('yearawalWork').value;
    let dateakhirWorkVal = document.getElementById('dateakhirWork').value;
    let yearakhirWorkVal = document.getElementById('yearakhirWork').value;
    let deskripsiWorkVal = document.getElementById('deskripsiWork').value;

    let removeSpaceWork = jobVal.replace(" ", "-");
    let removeSpaceWork2 = dateakhirWorkVal.replace(" ", "-");
    let removeSpaceWork3 = yearakhirWorkVal.replace(" ", "-");

    let idWork = removeSpaceWork + removeSpaceWork3 + removeSpaceWork2;

    if (jobVal === "" || companyVal === "" || dateawalWorkVal === "" ||
        yearawalWorkVal === "" || dateakhirWorkVal === "" || yearakhirWorkVal === "") {
        $('#modalWork').modal('show');
    } else {
        ArrWork.push(idWork);
        var uniqueNames = getUnique(ArrWork);
        if (hasDuplicates(ArrWork)) {
            // if value name school and end date is same 
            $('#SameWork').modal('show');
            ArrWork = uniqueNames.slice(0);           
        } else if (uniqueNames.length === 1) {
            work++;

            // push name job
            var nameJob = document.createElement("div");
            nameJob.setAttribute("id", `job-id-${idWork}`);
            nameJob.innerHTML += `<input id="job-val-${idWork}" value="${jobVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameJob').append(nameJob);

            // push name company
            var nameCompany = document.createElement("div");
            nameCompany.setAttribute("id", `company-id-${idWork}`);
            nameCompany.innerHTML += `<input id="company-val-${idWork}" value="${companyVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCompany').append(nameCompany);

            // push date begin work
            var beginWork = document.createElement("div");
            beginWork.setAttribute("id", `begin-id-${idWork}`);
            beginWork.innerHTML += `<input id="begin-work-${idWork}" value="${yearawalWorkVal + dateawalWorkVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentBeginWork').append(beginWork);

            // push date end work
            var endWork= document.createElement("div");
            endWork.setAttribute("id", `end-id-${idWork}`);
            endWork.innerHTML += `<input id="end-work-${idWork}" value="${yearakhirWorkVal + dateakhirWorkVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentEndWork').append(endWork);

            // push deskripsi work
            var deskripsiWork = document.createElement("div");
            deskripsiWork.setAttribute("id", `deskripsi-id-${idWork}`);
            deskripsiWork.innerHTML += `<input id="deskripsi-work-${idWork}" value="${deskripsiWorkVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiWork').append(deskripsiWork);

            //push button remove work
            var btnRemoveWork = document.createElement("div");
            btnRemoveWork.setAttribute("id", `btnRemoveWork-id-${idWork}`);
            btnRemoveWork.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeWork('${idWork}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveWork').append(btnRemoveWork);

            document.getElementById('job').value = "";
            document.getElementById('company').value = "";
            document.getElementById('dateawalWork').value = "";
            document.getElementById('yearawalWork').value = "";
            document.getElementById('dateakhirWork').value = "";
            document.getElementById('yearakhirWork').value = "";
            document.getElementById('deskripsiWork').value = "";

            $("#btnCloseAddWork").hide();

            if (work == 1) {
                // display button save if array length === 1
                document.getElementById('parentBtnSaveWork').innerHTML += `<button id="btnSaveWork" onclick="saveWork();" style="margin-top: 25px;" type="button" class="btn btn-success">Save Job</button>`
            } else {
                $("#btnSaveWork").show();
            }
        } else{
            // push name job
            var nameJob = document.createElement("div");
            nameJob.setAttribute("id", `job-id-${idWork}`);
            nameJob.innerHTML += `<input id="job-val-${idWork}" value="${jobVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameJob').append(nameJob);

            // push name company
            var nameCompany = document.createElement("div");
            nameCompany.setAttribute("id", `company-id-${idWork}`);
            nameCompany.innerHTML += `<input id="company-val-${idWork}" value="${companyVal}" class="form-control" style="font-weight: bold;" disabled><br>`;
            document.getElementById('parentNameCompany').append(nameCompany);

            // push date begin work
            var beginWork = document.createElement("div");
            beginWork.setAttribute("id", `begin-id-${idWork}`);
            beginWork.innerHTML += `<input id="begin-work-${idWork}" value="${yearawalWorkVal + dateawalWorkVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentBeginWork').append(beginWork);

            // push date end work
            var endWork= document.createElement("div");
            endWork.setAttribute("id", `end-id-${idWork}`);
            endWork.innerHTML += `<input id="end-work-${idWork}" value="${yearakhirWorkVal + dateakhirWorkVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentEndWork').append(endWork);

            // push deskripsi work
            var deskripsiWork = document.createElement("div");
            deskripsiWork.setAttribute("id", `deskripsi-id-${idWork}`);
            deskripsiWork.innerHTML += `<input id="deskripsi-work-${idWork}" value="${deskripsiWorkVal}" class="form-control" style="font-weight: bold;" hidden disabled><br>`;
            document.getElementById('parentDeskripsiWork').append(deskripsiWork);

            //push button remove work
            var btnRemoveWork = document.createElement("div");
            btnRemoveWork.setAttribute("id", `btnRemoveWork-id-${idWork}`);
            btnRemoveWork.innerHTML += `<button style="background-color: transparent;" type="button" class="btn" onclick="removeWork('${idWork}')"><i class="fa fa-close"></i></button><br><br>`
            document.getElementById('parentBtnRemoveWork').append(btnRemoveWork);

            document.getElementById('job').value = "";
            document.getElementById('company').value = "";
            document.getElementById('dateawalWork').value = "";
            document.getElementById('yearawalWork').value = "";
            document.getElementById('dateakhirWork').value = "";
            document.getElementById('yearakhirWork').value = "";
            document.getElementById('deskripsiWork').value = "";

            $("#btnSaveWork").show();
            $("#btnCloseWork").hide();
        }
    }
}