var controlpdf = document.getElementById("filepdf");
controlpdf.addEventListener("change", function(event) {
    // When the control has changed, there are new files
    var files = controlpdf.files;


    if (files[0] != null) {
        var img = new Image();
        console.log("Filename: " + files[0].name);
        console.log("Type: " + files[0].type);
        console.log("Size: " + files[0].size + " bytes");
        if (files[0].size > 1000000) {
            //File too big
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'File CV too Big!',
            })
            document.getElementById("registerButton").disabled = true;
        } else {
            var fileReader = new FileReader();
            var type;
            fileReader.onloadend = function(e) {
                var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                var header = "";
                for (var i = 0; i < arr.length; i++) {
                    header += arr[i].toString(16);
                }
                console.log(header);

                // Check the file signature against known types
                switch (header) {
                    case "25504446":
                        console.log("enabled");
                        document.getElementById("registerButton").disabled = false; // Or you can use the blob.type as fallback
                        break;
                    default:
                        //Files is not jpg/jpeg/png
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'File upload CV must PDF!',
                        })
                        console.log("disabled");
                        document.getElementById("registerButton").disabled = true; // Or you can use the blob.type as fallback
                        break;
                }

            };
            fileReader.readAsArrayBuffer(files[0]);

            console.log(files[0].type);
        }

    } else {
        document.getElementById("registerButton").disabled = false;
    }

}, false);