let addTraining = function() {
    let rowIndex = document.querySelectorAll('.itemTraining').length; //we can add mock class to each movie-row

    $('#trainingList').append('<div class="itemTraining" style="margin-top: 40px;">' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-5 renderTraining">' +
        '<label class="font-weight-bold">Training Name<sup>*</sup></label>' +
        `<div>` +
        '<input id="trainings' + rowIndex + '.id_train" name="trainings[' + rowIndex + '].id_train" type="text" class="form-control" value="0" hidden>' +
        '<input type="text" class="form-control" id="trainings' + rowIndex + '.ntraining" name="trainings[' + rowIndex + '].ntraining" placeholder="Name Training">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-5 renderTraining">' +
        '<label class="font-weight-bold">Company<sup>*</sup></label>' +
        `<div>` +
        '<input type="text" class="form-control" id="trainings' + rowIndex + '.company" name="trainings[' + rowIndex + '].company" placeholder="Company">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-4 renderTraining">' +
        '<label class="font-weight-bold">Start Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="trainings' + rowIndex + '.date_awal" name="trainings[' + rowIndex + '].date_awal" placeholder="Address">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-4 renderTraining">' +
        '<label class="font-weight-bold">End Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="trainings' + rowIndex + '.date_akhir" name="trainings[' + rowIndex + '].date_akhir" placeholder="Statement">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-8 renderTraining">' +
        '<label>Accomplishments or descriptions (optional)</label>' +
        `<div>` +
        '<textarea type="textarea" class="form-control" id="trainings' + rowIndex + '.description" name="trainings[' + rowIndex + '].description" placeholder="e.g Valedictorian, Honorable Mention, etc."></textarea>' +
        `</div>` +
        '</div>' +
        '<div class="col-md-2">' +
        '<a style="color: white; margin-top: 45px;" class="btn linkdelete"><i style="color: red;" class="fa fa-close"></i></a>' +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '</div>')

    $('#trainingList .linkdelete').click(function() {
        $(this).parent().parent().parent().remove();

        let tes = $('.itemTraining .form-row .renderTraining div').children();
        console.log(tes);

        tes.each(function(i, v) {
            if (i % 6 == 0) {
                v.id = 'trainings' + (i / 6) + '.id_train';
                v.setAttribute('name', 'trainings' + '[' + (i / 6) + '].id_train');
            } else if (i % 6 == 1) {
                v.id = 'trainings' + Math.floor(i / 6) + '.ntraining';
                v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 6) + '].ntraining');
            } else if (i % 6 == 2) {
                v.id = 'trainings' + Math.floor(i / 6) + '.company';
                v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 6) + '].company');
            } else if (i % 6 == 3) {
                v.id = 'trainings' + Math.floor(i / 6) + '.date_awal';
                v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 6) + '].date_awal');
            } else if (i % 6 == 4) {
                v.id = 'trainings' + Math.floor(i / 5) + '.date_akhir';
                v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 5) + '].date_akhir');
            } else if (i % 6 == 5) {
                v.id = 'trainings' + Math.floor(i / 5) + '.description';
                v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 5) + '].description');
            }
        })

    });
};

$('#trainingList .linkdelete').click(function() {
    $(this).parent().parent().parent().remove();

    let tes = $('.itemTraining .form-row .renderTraining div').children();
    console.log(tes);

    tes.each(function(i, v) {
        if (i % 6 == 0) {
            v.id = 'trainings' + (i / 6) + '.id_train';
            v.setAttribute('name', 'trainings' + '[' + (i / 6) + '].id_train');
        } else if (i % 6 == 1) {
            v.id = 'trainings' + Math.floor(i / 6) + '.ntraining';
            v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 6) + '].ntraining');
        } else if (i % 6 == 2) {
            v.id = 'trainings' + Math.floor(i / 6) + '.company';
            v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 6) + '].company');
        } else if (i % 6 == 3) {
            v.id = 'trainings' + Math.floor(i / 6) + '.date_awal';
            v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 6) + '].date_awal');
        } else if (i % 6 == 4) {
            v.id = 'trainings' + Math.floor(i / 5) + '.date_akhir';
            v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 5) + '].date_akhir');
        } else if (i % 6 == 5) {
            v.id = 'trainings' + Math.floor(i / 5) + '.description';
            v.setAttribute('name', 'trainings' + '[' + Math.floor(i / 5) + '].description');
        }
    })

});