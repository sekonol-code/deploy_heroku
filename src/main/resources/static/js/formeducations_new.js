let addEducations = function() {
    let rowIndex = document.querySelectorAll('.itemEducations').length; //we can add mock class to each movie-row

    $('#educationsList').append('<div class="itemEducations" style="margin-top: 40px;">' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-5 renderEducation">' +
        '<label class="font-weight-bold">School<sup>*</sup></label>' +
        `<div>` +
        '<input id="educations' + rowIndex + '.id_education" name="educations[' + rowIndex + '].id_education" type="text" class="form-control" value="0" hidden>' +
        '<input type="text" class="form-control" id="educations' + rowIndex + '.school" name="educations[' + rowIndex + '].school" placeholder="Name School">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-5 renderEducation">' +
        '<label class="font-weight-bold">Major<sup>*</sup></label>' +
        `<div>` +
        '<input type="text" class="form-control" id="educations' + rowIndex + '.major" name="educations[' + rowIndex + '].major" placeholder="Major">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-4 renderEducation">' +
        '<label class="font-weight-bold">Start Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="educations' + rowIndex + '.date_awal" name="educations[' + rowIndex + '].date_awal" placeholder="Address">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-4 renderEducation">' +
        '<label class="font-weight-bold">End Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="educations' + rowIndex + '.date_akhir" name="educations[' + rowIndex + '].date_akhir" placeholder="Statement">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-5 renderEducation">' +
        '<label class="font-weight-bold">GPA<sup>*</sup></label>' +
        `<div>` +
        '<input type="text" class="form-control" id="educations' + rowIndex + '.description" name="educations[' + rowIndex + '].description" placeholder="e.g 4.00"></input>' +
        `</div>` +
        '</div>' +
        '<div class="col-md-2">' +
        '<a style="color: white; margin-top: 30px;" class="btn linkdelete"><i style="color: red;" class="fa fa-close"></i></a>' +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '</div>')

    $('#educationsList .linkdelete').click(function() {
        $(this).parent().parent().parent().remove();

        let rowIndex = document.querySelectorAll('.itemEducations').length;
        if (rowIndex == 0) {
            addEducations();
        }

        let tes = $('.itemEducations .form-row .renderEducation div').children();
        console.log(tes);

        tes.each(function(i, v) {
            if (i % 6 == 0) {
                v.id = 'educations' + (i / 6) + '.id_education';
                v.setAttribute('name', 'educations' + '[' + (i / 6) + '].id_education');
            } else if (i % 6 == 1) {
                v.id = 'educations' + Math.floor(i / 6) + '.school';
                v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].school');
            } else if (i % 6 == 2) {
                v.id = 'educations' + Math.floor(i / 6) + '.major';
                v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].major');
            } else if (i % 6 == 3) {
                v.id = 'educations' + Math.floor(i / 6) + '.date_awal';
                v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].date_awal');
            } else if (i % 6 == 4) {
                v.id = 'educations' + Math.floor(i / 6) + '.date_akhir';
                v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].date_akhir');
            } else if (i % 6 == 5) {
                v.id = 'educations' + Math.floor(i / 6) + '.description';
                v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].description');
            }
        })

    });
};

$('#educationsList .linkdelete').click(function() {
    $(this).parent().parent().parent().remove();

    let rowIndex = document.querySelectorAll('.itemEducations').length;
    if (rowIndex == 0) {
        addEducations();
    }

    let tes = $('.itemEducations .form-row .renderEducation div').children();
    console.log(tes);

    tes.each(function(i, v) {
        if (i % 6 == 0) {
            v.id = 'educations' + (i / 6) + '.id_education';
            v.setAttribute('name', 'educations' + '[' + (i / 6) + '].id_education');
        } else if (i % 6 == 1) {
            v.id = 'educations' + Math.floor(i / 6) + '.school';
            v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].school');
        } else if (i % 6 == 2) {
            v.id = 'educations' + Math.floor(i / 6) + '.major';
            v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].major');
        } else if (i % 6 == 3) {
            v.id = 'educations' + Math.floor(i / 6) + '.date_awal';
            v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].date_awal');
        } else if (i % 6 == 4) {
            v.id = 'educations' + Math.floor(i / 6) + '.date_akhir';
            v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].date_akhir');
        } else if (i % 6 == 5) {
            v.id = 'educations' + Math.floor(i / 6) + '.description';
            v.setAttribute('name', 'educations' + '[' + Math.floor(i / 6) + '].description');
        }
    })

});

$(window).on("load", function() {
    let rowIndex = document.querySelectorAll('.itemEducations').length; //we can add mock class to each movie-row
    if (rowIndex == 0) {
        addEducations();
    }
})