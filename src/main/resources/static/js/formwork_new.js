let addWork = function() {
    let rowIndex = document.querySelectorAll('.itemWork').length; //we can add mock class to each movie-row

    $('#workList').append('<div class="itemWork" style="margin-top: 40px;">' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-5 renderWork">' +
        '<label class="font-weight-bold">Job Title<sup>*</sup></label>' +
        `<div>` +
        '<input id="workexs' + rowIndex + '.id_workex" name="workexs[' + rowIndex + '].id_workex" type="text" class="form-control" value="0" hidden>' +
        '<input type="text" class="form-control" id="workexs' + rowIndex + '.job" name="workexs[' + rowIndex + '].job" placeholder="Job Title">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-5 renderWork">' +
        '<label class="font-weight-bold">Company<sup>*</sup></label>' +
        `<div>` +
        '<input type="text" class="form-control" id="workexs' + rowIndex + '.company" name="workexs[' + rowIndex + '].company" placeholder="Company">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-4 renderWork">' +
        '<label class="font-weight-bold">Start Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="workexs' + rowIndex + '.date_awal" name="workexs[' + rowIndex + '].date_awal" placeholder="Address">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-4 renderWork">' +
        '<label class="font-weight-bold">End Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="workexs' + rowIndex + '.date_akhir" name="workexs[' + rowIndex + '].date_akhir" placeholder="Statement">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-8 renderWork">' +
        '<label>Job descriptions<sup>*</sup></label>' +
        `<div>` +
        '<textarea type="textarea" class="form-control" id="workexs' + rowIndex + '.description" name="workexs[' + rowIndex + '].description" placeholder="e.g Valedictorian, Honorable Mention, etc."></textarea>' +
        `</div>` +
        '</div>' +
        '<div class="col-md-2">' +
        '<a style="color: white; margin-top: 45px;" class="btn linkdelete"><i style="color: red;" class="fa fa-close"></i></a>' +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '</div>')

    $('#workList .linkdelete').click(function() {
        $(this).parent().parent().parent().remove();

        let tes = $('.itemWork .form-row .renderWork div').children();
        console.log(tes);

        let rowIndex = document.querySelectorAll('.itemWork').length; //we can add mock class to each movie-row
        if (rowIndex == 0 && id_job > 1) {
            addWork();
        }

        tes.each(function(i, v) {
            if (i % 6 == 0) {
                v.id = 'workexs' + (i / 6) + '.id_workex';
                v.setAttribute('name', 'workexs' + '[' + (i / 6) + '].id_workex');
            } else if (i % 6 == 1) {
                v.id = 'workexs' + Math.floor(i / 6) + '.job';
                v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].job');
            } else if (i % 6 == 2) {
                v.id = 'workexs' + Math.floor(i / 6) + '.company';
                v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].company');
            } else if (i % 6 == 3) {
                v.id = 'workexs' + Math.floor(i / 6) + '.date_awal';
                v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].date_awal');
            } else if (i % 6 == 4) {
                v.id = 'workexs' + Math.floor(i / 6) + '.date_akhir';
                v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].date_akhir');
            } else if (i % 6 == 5) {
                v.id = 'workexs' + Math.floor(i / 6) + '.description';
                v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].description');
            }
        })

    });
};

$('#workList .linkdelete').click(function() {
    $(this).parent().parent().parent().remove();

    let rowIndex = document.querySelectorAll('.itemWork').length; //we can add mock class to each movie-row
    if (rowIndex == 0 && id_job > 1) {
        addWork();
        console.log("add delete");
    }

    let tes = $('.itemWork .form-row .renderWork div').children();
    console.log(tes);

    tes.each(function(i, v) {
        if (i % 6 == 0) {
            v.id = 'workexs' + (i / 6) + '.id_workex';
            v.setAttribute('name', 'workexs' + '[' + (i / 6) + '].id_workex');
        } else if (i % 6 == 1) {
            v.id = 'workexs' + Math.floor(i / 6) + '.job';
            v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].job');
        } else if (i % 6 == 2) {
            v.id = 'workexs' + Math.floor(i / 6) + '.company';
            v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].company');
        } else if (i % 6 == 3) {
            v.id = 'workexs' + Math.floor(i / 6) + '.date_awal';
            v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].date_awal');
        } else if (i % 6 == 4) {
            v.id = 'workexs' + Math.floor(i / 6) + '.date_akhir';
            v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].date_akhir');
        } else if (i % 6 == 5) {
            v.id = 'workexs' + Math.floor(i / 6) + '.description';
            v.setAttribute('name', 'workexs' + '[' + Math.floor(i / 6) + '].description');
        }
    })

});
$(window).on("load", function() {
    let rowIndex = document.querySelectorAll('.itemWork').length; //we can add mock class to each movie-row
    if (rowIndex == 0 && id_job > 1) {
        addWork();
    }
})