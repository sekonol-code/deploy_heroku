let addCertificate = function() {
    let rowIndex = document.querySelectorAll('.itemCertificate').length; //we can add mock class to each movie-row

    $('#certificateList').append('<div class="itemCertificate" style="margin-top: 40px;">' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-5 renderCertificate">' +
        '<label class="font-weight-bold">Certificate Name<sup>*</sup></label>' +
        `<div>` +
        '<input id="certificates' + rowIndex + '.id_certificate" name="certificates[' + rowIndex + '].id_certificate" type="text" class="form-control" value="0" hidden>' +
        '<input type="text" class="form-control" id="certificates' + rowIndex + '.nama_certificate" name="certificates[' + rowIndex + '].nama_certificate" placeholder="Name Training">' +
        `</div>` +
        '</div>' +
        '<div class="form-group col-md-5 renderCertificate">' +
        '<label class="font-weight-bold">Company<sup>*</sup></label>' +
        `<div>` +
        '<input type="text" class="form-control" id="certificates' + rowIndex + '.company" name="certificates[' + rowIndex + '].company" placeholder="Company">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-4 renderCertificate">' +
        '<label class="font-weight-bold">Date<sup>*</sup></label>' +
        `<div>` +
        '<input type="date" class="form-control" id="certificates' + rowIndex + '.date" name="certificates[' + rowIndex + '].date" placeholder="Address">' +
        `</div>` +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '<div class="form-row">' +
        '<div class="col-md-1"></div>' +
        '<div class="form-group col-md-8 renderCertificate">' +
        '<label>Accomplishments or descriptions (optional)</label>' +
        `<div>` +
        '<textarea type="textarea" class="form-control" id="certificates' + rowIndex + '.description" name="certificates[' + rowIndex + '].description" placeholder="e.g Valedictorian, Honorable Mention, etc."></textarea>' +
        `</div>` +
        '</div>' +
        '<div class="col-md-2">' +
        '<a style="color: white; margin-top: 45px;" class="btn linkdelete"><i style="color: red;" class="fa fa-close"></i></a>' +
        '</div>' +
        '<div class="col-md-1"></div>' +
        '</div>' +
        '</div>')

    $('#certificateList .linkdelete').click(function() {
        $(this).parent().parent().parent().remove();

        let tes = $('.itemCertificate .form-row .renderCertificate div').children();
        console.log(tes);

        tes.each(function(i, v) {
            if (i % 5 == 0) {
                v.id = 'certificates' + (i / 5) + '.id_certificate';
                v.setAttribute('name', 'certificates' + '[' + (i / 5) + '].id_certificate');
            } else if (i % 5 == 1) {
                v.id = 'certificates' + Math.floor(i / 5) + '.nama_certificate';
                v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].nama_certificate');
            } else if (i % 5 == 2) {
                v.id = 'certificates' + Math.floor(i / 5) + '.company';
                v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].company');
            } else if (i % 5 == 3) {
                v.id = 'certificates' + Math.floor(i / 5) + '.date';
                v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].date');
            } else if (i % 5 == 4) {
                v.id = 'certificates' + Math.floor(i / 5) + '.description';
                v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].description');
            }
        })

    });
};

$('#certificateList .linkdelete').click(function() {
    $(this).parent().parent().parent().remove();

    let tes = $('.itemCertificate .form-row .renderCertificate div').children();
    console.log(tes);

    tes.each(function(i, v) {
        if (i % 5 == 0) {
            v.id = 'certificates' + (i / 5) + '.id_certificate';
            v.setAttribute('name', 'certificates' + '[' + (i / 5) + '].id_certificate');
        } else if (i % 5 == 1) {
            v.id = 'certificates' + Math.floor(i / 5) + '.nama_certificate';
            v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].nama_certificate');
        } else if (i % 5 == 2) {
            v.id = 'certificates' + Math.floor(i / 5) + '.company';
            v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].company');
        } else if (i % 5 == 3) {
            v.id = 'certificates' + Math.floor(i / 5) + '.date';
            v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].date');
        } else if (i % 5 == 4) {
            v.id = 'certificates' + Math.floor(i / 5) + '.description';
            v.setAttribute('name', 'certificates' + '[' + Math.floor(i / 5) + '].description');
        }
    })

});