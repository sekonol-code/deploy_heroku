$("#btnEditAplicants").hide();
$("#btnCloseAplicants").hide();
$("#btnCloseAddAplicants").hide();
$("#icon-check-aplicants").hide();

$("#btnAddAplicants").click(function() {
    $("#btnCloseAddAplicants").show();
    $("#btnAddAplicants").hide();
});

$("#btnCloseAddAplicants").click(function() {
    $("#btnAddAplicants").show();
    $("#btnCloseAddAplicants").hide();
});

$("#btnEditAplicants").click(function() {
    $("#btnCloseAplicants").show();
    $("#btnEditAplicants").hide();
    $("#icon-check-aplicants").hide();
});

$("#btnCloseAplicants").click(function() {
    $("#btnEditAplicants").show();
    $("#btnCloseAplicants").hide();
    $("#icon-check-aplicants").show();
});

function pushApplicants() {
    var nameAplicansVal = document.getElementById('nameAplicans').value;
    var emailAplicansVal = document.getElementById('emailAplicans').value;
    var addressAplicansVal = document.getElementById('addressAplicans').value;
    var contactAplicansVal = document.getElementById('contactAplicans').value;
    var customFileImageVal = document.getElementById('filejpg').value;
    var customFilePdfVal = document.getElementById('filepdf').value;

    if (nameAplicansVal === "" || emailAplicansVal === "" || addressAplicansVal === "" ||
        contactAplicansVal === "" || customFileImageVal === "" || customFilePdfVal === "") {
        $('#modalAplicants').modal('show');
        $('#modalsaveAplicant').modal('hide');
        $("#btnSaveAplicants").attr("data-toggle", "");
        $("#btnSaveAplicants").attr("data-target", "");
        $("#btnSaveAplicants").attr("aria-expanded", "");
        $("#btnSaveAplicants").attr("aria-controls", "");
    } else {
        $("#btnSaveAplicants").attr("data-toggle", "collapse");
        $("#btnSaveAplicants").attr("data-target", "#collapseThree");
        $("#btnSaveAplicants").attr("aria-expanded", "false");
        $("#btnSaveAplicants").attr("aria-controls", "collapseThree");
        $('#modalsaveAplicant').modal('hide');
        $("#icon-check-aplicants").show();
        $("#btnCloseAplicants").hide();
        $("#btnCloseAddAplicants").hide();
        $("#btnEditAplicants").show();
    }
}