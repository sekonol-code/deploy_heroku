--
-- zzoqhhnacdntpdQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

-- Started on 2020-01-10 08:39:55

SET statement_timeout
= 0;
SET lock_timeout
= 0;
SET idle_in_transaction_session_timeout
= 0;
SET client_encoding
= 'UTF8';
SET standard_conforming_strings
= on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies
= false;
SET xmloption
= content;
SET client_min_messages
= warning;
SET row_security
= off;

SET default_tablespace
= '';

SET default_with_oids
= false;

--
-- TOC entry 196 (class 1259 OID 29717)
-- Name: applicants; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.applicants
(
    id_ap integer NOT NULL,
    nama text NOT NULL,
    alamat text NOT NULL,
    email text NOT NULL,
    contact text NOT NULL,
    data_gambar bytea,
    data_cv bytea,
    link_gambar text,
    link_cv text,
    personal_statement text
);


ALTER TABLE public.applicants OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 197 (class 1259 OID 29723)
-- Name: applicants_access_job; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.applicants_access_job
(
    id_applicants_access_job integer NOT NULL,
    id_job integer NOT NULL,
    id_ap integer NOT NULL,
    status integer NOT NULL
);


ALTER TABLE public.applicants_access_job OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 198 (class 1259 OID 29726)
-- Name: applicants_access_job_id_applicants_access_job_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.applicants_access_job_id_applicants_access_job_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.applicants_access_job_id_applicants_access_job_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3009 (class 0 OID 0)
-- Dependencies: 198
-- Name: applicants_access_job_id_applicants_access_job_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.applicants_access_job_id_applicants_access_job_seq
OWNED BY public.applicants_access_job.id_applicants_access_job;


--
-- TOC entry 199 (class 1259 OID 29728)
-- Name: applicants_id_ap_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.applicants_id_ap_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.applicants_id_ap_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3010 (class 0 OID 0)
-- Dependencies: 199
-- Name: applicants_id_ap_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.applicants_id_ap_seq
OWNED BY public.applicants.id_ap;


--
-- TOC entry 200 (class 1259 OID 29730)
-- Name: certificate; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.certificate
(
    id_certificate integer NOT NULL,
    id_ap integer NOT NULL,
    nama_certificate text,
    date date,
    company text,
    description text
);


ALTER TABLE public.certificate OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 201 (class 1259 OID 29736)
-- Name: certificate_id_certificate_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.certificate_id_certificate_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.certificate_id_certificate_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3011 (class 0 OID 0)
-- Dependencies: 201
-- Name: certificate_id_certificate_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.certificate_id_certificate_seq
OWNED BY public.certificate.id_certificate;


--
-- TOC entry 202 (class 1259 OID 29738)
-- Name: description; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.description
(
    id_desc integer NOT NULL,
    id_job integer NOT NULL,
    nama_desc text NOT NULL
);


ALTER TABLE public.description OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 203 (class 1259 OID 29744)
-- Name: description_id_desc_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.description_id_desc_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.description_id_desc_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3012 (class 0 OID 0)
-- Dependencies: 203
-- Name: description_id_desc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.description_id_desc_seq
OWNED BY public.description.id_desc;


--
-- TOC entry 204 (class 1259 OID 29746)
-- Name: educations; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.educations
(
    id_education integer NOT NULL,
    id_ap integer NOT NULL,
    date_awal date NOT NULL,
    date_akhir date NOT NULL,
    school text NOT NULL,
    major text NOT NULL,
    description text
);


ALTER TABLE public.educations OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 205 (class 1259 OID 29752)
-- Name: educations_id_education_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.educations_id_education_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.educations_id_education_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3013 (class 0 OID 0)
-- Dependencies: 205
-- Name: educations_id_education_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.educations_id_education_seq
OWNED BY public.educations.id_education;


--
-- TOC entry 206 (class 1259 OID 29754)
-- Name: job; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.job
(
    id_job integer NOT NULL,
    nama_job text NOT NULL,
    is_active integer NOT NULL
);


ALTER TABLE public.job OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 207 (class 1259 OID 29760)
-- Name: job_id_job_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.job_id_job_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.job_id_job_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3014 (class 0 OID 0)
-- Dependencies: 207
-- Name: job_id_job_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.job_id_job_seq
OWNED BY public.job.id_job;


--
-- TOC entry 208 (class 1259 OID 29762)
-- Name: menu; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.menu
(
    id_menu integer NOT NULL,
    nama_menu text NOT NULL,
    is_active integer
);


ALTER TABLE public.menu OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 224 (class 1259 OID 29949)
-- Name: projects; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.projects
(
    id_project integer NOT NULL,
    id_ap integer NOT NULL,
    nama_project text,
    posisi_project text,
    date_awal date,
    date_akhir date
);


ALTER TABLE public.projects OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 223 (class 1259 OID 29947)
-- Name: projects_id_project_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.projects_id_project_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_project_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3015 (class 0 OID 0)
-- Dependencies: 223
-- Name: projects_id_project_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.projects_id_project_seq
OWNED BY public.projects.id_project;


--
-- TOC entry 209 (class 1259 OID 29768)
-- Name: qualification; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.qualification
(
    id_qualification integer NOT NULL,
    id_job integer NOT NULL,
    nama_qualification text NOT NULL
);


ALTER TABLE public.qualification OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 210 (class 1259 OID 29774)
-- Name: qualification_id_qualification_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.qualification_id_qualification_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qualification_id_qualification_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3016 (class 0 OID 0)
-- Dependencies: 210
-- Name: qualification_id_qualification_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.qualification_id_qualification_seq
OWNED BY public.qualification.id_qualification;


--
-- TOC entry 211 (class 1259 OID 29776)
-- Name: roles; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.roles
(
    id_role integer NOT NULL,
    role character varying(50) NOT NULL
);


ALTER TABLE public.roles OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 212 (class 1259 OID 29779)
-- Name: skills; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.skills
(
    id_skill integer NOT NULL,
    id_ap integer NOT NULL,
    skill text,
    level text
);


ALTER TABLE public.skills OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 213 (class 1259 OID 29785)
-- Name: skills_id_skill_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.skills_id_skill_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skills_id_skill_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3017 (class 0 OID 0)
-- Dependencies: 213
-- Name: skills_id_skill_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.skills_id_skill_seq
OWNED BY public.skills.id_skill;


--
-- TOC entry 214 (class 1259 OID 29787)
-- Name: spring_session; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.spring_session
(
    primary_id character(36) NOT NULL,
    session_id character varying(36) NOT NULL,
    creation_time bigint NOT NULL,
    last_access_time bigint NOT NULL,
    max_inactive_interval integer NOT NULL,
    expiry_time bigint NOT NULL,
    principal_name character varying(100)
);


ALTER TABLE public.spring_session OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 215 (class 1259 OID 29790)
-- Name: spring_session_attributes; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.spring_session_attributes
(
    session_primary_id character(36) NOT NULL,
    attribute_name character varying(200) NOT NULL,
    attribute_bytes bytea NOT NULL
);


ALTER TABLE public.spring_session_attributes OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 216 (class 1259 OID 29796)
-- Name: submenu; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.submenu
(
    id_sub_menu integer NOT NULL,
    id_menu integer NOT NULL,
    nama_sub_menu text NOT NULL,
    url text,
    icon text,
    is_active integer
);


ALTER TABLE public.submenu OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 217 (class 1259 OID 29802)
-- Name: training; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.training
(
    id_training integer NOT NULL,
    id_ap integer NOT NULL,
    ntraining text,
    date_awal date,
    date_akhir date,
    description text,
    company text
);


ALTER TABLE public.training OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 218 (class 1259 OID 29808)
-- Name: training_id_training_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.training_id_training_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.training_id_training_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 218
-- Name: training_id_training_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.training_id_training_seq
OWNED BY public.training.id_training;


--
-- TOC entry 219 (class 1259 OID 29810)
-- Name: users; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.users
(
    id_users integer NOT NULL,
    id_role integer NOT NULL,
    uname text NOT NULL,
    email text NOT NULL,
    upass text NOT NULL,
    is_active integer
);


ALTER TABLE public.users OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 220 (class 1259 OID 29816)
-- Name: users_access_menu; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.users_access_menu
(
    id_users_access_menu integer NOT NULL,
    id_role integer NOT NULL,
    id_menu integer NOT NULL
);


ALTER TABLE public.users_access_menu OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 221 (class 1259 OID 29819)
-- Name: workex; Type: TABLE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE TABLE public.workex
(
    id_workex integer NOT NULL,
    id_ap integer NOT NULL,
    date_awal date,
    date_akhir date,
    job text,
    description text,
    company text
);


ALTER TABLE public.workex OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 222 (class 1259 OID 29825)
-- Name: workex_id_workex_seq; Type: SEQUENCE; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE SEQUENCE public.workex_id_workex_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workex_id_workex_seq OWNER TO zzoqhhnacdntpd;

--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 222
-- Name: workex_id_workex_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER SEQUENCE public.workex_id_workex_seq
OWNED BY public.workex.id_workex;


--
-- TOC entry 2787 (class 2604 OID 29827)
-- Name: applicants id_ap; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.applicants
ALTER COLUMN id_ap
SET
DEFAULT nextval
('public.applicants_id_ap_seq'::regclass);


--
-- TOC entry 2788 (class 2604 OID 29828)
-- Name: applicants_access_job id_applicants_access_job; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.applicants_access_job
ALTER COLUMN id_applicants_access_job
SET
DEFAULT nextval
('public.applicants_access_job_id_applicants_access_job_seq'::regclass);


--
-- TOC entry 2789 (class 2604 OID 29829)
-- Name: certificate id_certificate; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.certificate
ALTER COLUMN id_certificate
SET
DEFAULT nextval
('public.certificate_id_certificate_seq'::regclass);


--
-- TOC entry 2790 (class 2604 OID 29830)
-- Name: description id_desc; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.description
ALTER COLUMN id_desc
SET
DEFAULT nextval
('public.description_id_desc_seq'::regclass);


--
-- TOC entry 2791 (class 2604 OID 29831)
-- Name: educations id_education; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.educations
ALTER COLUMN id_education
SET
DEFAULT nextval
('public.educations_id_education_seq'::regclass);


--
-- TOC entry 2792 (class 2604 OID 29832)
-- Name: job id_job; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.job
ALTER COLUMN id_job
SET
DEFAULT nextval
('public.job_id_job_seq'::regclass);


--
-- TOC entry 2797 (class 2604 OID 29952)
-- Name: projects id_project; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.projects
ALTER COLUMN id_project
SET
DEFAULT nextval
('public.projects_id_project_seq'::regclass);


--
-- TOC entry 2793 (class 2604 OID 29833)
-- Name: qualification id_qualification; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.qualification
ALTER COLUMN id_qualification
SET
DEFAULT nextval
('public.qualification_id_qualification_seq'::regclass);


--
-- TOC entry 2794 (class 2604 OID 29834)
-- Name: skills id_skill; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.skills
ALTER COLUMN id_skill
SET
DEFAULT nextval
('public.skills_id_skill_seq'::regclass);


--
-- TOC entry 2795 (class 2604 OID 29835)
-- Name: training id_training; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.training
ALTER COLUMN id_training
SET
DEFAULT nextval
('public.training_id_training_seq'::regclass);


--
-- TOC entry 2796 (class 2604 OID 29836)
-- Name: workex id_workex; Type: DEFAULT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.workex
ALTER COLUMN id_workex
SET
DEFAULT nextval
('public.workex_id_workex_seq'::regclass);

--
-- TOC entry 3020 (class 0 OID 0)
-- Dependencies: 198
-- Name: applicants_access_job_id_applicants_access_job_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.applicants_access_job_id_applicants_access_job_seq', 1, false);


--
-- TOC entry 3021 (class 0 OID 0)
-- Dependencies: 199
-- Name: applicants_id_ap_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.applicants_id_ap_seq', 1, false);


--
-- TOC entry 3022 (class 0 OID 0)
-- Dependencies: 201
-- Name: certificate_id_certificate_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.certificate_id_certificate_seq', 1, false);


--
-- TOC entry 3023 (class 0 OID 0)
-- Dependencies: 203
-- Name: description_id_desc_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.description_id_desc_seq', 1, false);


--
-- TOC entry 3024 (class 0 OID 0)
-- Dependencies: 205
-- Name: educations_id_education_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.educations_id_education_seq', 1, false);


--
-- TOC entry 3025 (class 0 OID 0)
-- Dependencies: 207
-- Name: job_id_job_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.job_id_job_seq', 1, false);


--
-- TOC entry 3026 (class 0 OID 0)
-- Dependencies: 223
-- Name: projects_id_project_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.projects_id_project_seq', 1, false);


--
-- TOC entry 3027 (class 0 OID 0)
-- Dependencies: 210
-- Name: qualification_id_qualification_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.qualification_id_qualification_seq', 1, false);


--
-- TOC entry 3028 (class 0 OID 0)
-- Dependencies: 213
-- Name: skills_id_skill_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.skills_id_skill_seq', 1, false);


--
-- TOC entry 3029 (class 0 OID 0)
-- Dependencies: 218
-- Name: training_id_training_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.training_id_training_seq', 1, false);


--
-- TOC entry 3030 (class 0 OID 0)
-- Dependencies: 222
-- Name: workex_id_workex_seq; Type: SEQUENCE SET; Schema: public; Owner: zzoqhhnacdntpd
--

SELECT pg_catalog.setval('public.workex_id_workex_seq', 1, false);


--
-- TOC entry 2799 (class 2606 OID 29838)
-- Name: applicants PK_applicants; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.applicants
ADD CONSTRAINT "PK_applicants" PRIMARY KEY
(id_ap);


--
-- TOC entry 2801 (class 2606 OID 29840)
-- Name: applicants_access_job PK_applicants_access_job; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.applicants_access_job
ADD CONSTRAINT "PK_applicants_access_job" PRIMARY KEY
(id_applicants_access_job);


--
-- TOC entry 2803 (class 2606 OID 29842)
-- Name: certificate PK_certificate; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.certificate
ADD CONSTRAINT "PK_certificate" PRIMARY KEY
(id_certificate);


--
-- TOC entry 2805 (class 2606 OID 29844)
-- Name: description PK_description; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.description
ADD CONSTRAINT "PK_description" PRIMARY KEY
(id_desc);


--
-- TOC entry 2807 (class 2606 OID 29846)
-- Name: educations PK_educations; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.educations
ADD CONSTRAINT "PK_educations" PRIMARY KEY
(id_education);


--
-- TOC entry 2809 (class 2606 OID 29848)
-- Name: job PK_job; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.job
ADD CONSTRAINT "PK_job" PRIMARY KEY
(id_job);


--
-- TOC entry 2811 (class 2606 OID 29850)
-- Name: menu PK_menu; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.menu
ADD CONSTRAINT "PK_menu" PRIMARY KEY
(id_menu);


--
-- TOC entry 2838 (class 2606 OID 29957)
-- Name: projects PK_projects; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.projects
ADD CONSTRAINT "PK_projects" PRIMARY KEY
(id_project);


--
-- TOC entry 2813 (class 2606 OID 29852)
-- Name: qualification PK_qualification; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.qualification
ADD CONSTRAINT "PK_qualification" PRIMARY KEY
(id_qualification);


--
-- TOC entry 2815 (class 2606 OID 29854)
-- Name: roles PK_roles; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.roles
ADD CONSTRAINT "PK_roles" PRIMARY KEY
(id_role);


--
-- TOC entry 2817 (class 2606 OID 29856)
-- Name: skills PK_skills; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.skills
ADD CONSTRAINT "PK_skills" PRIMARY KEY
(id_skill);


--
-- TOC entry 2826 (class 2606 OID 29858)
-- Name: submenu PK_submenu; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.submenu
ADD CONSTRAINT "PK_submenu" PRIMARY KEY
(id_sub_menu);


--
-- TOC entry 2828 (class 2606 OID 29860)
-- Name: training PK_training; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.training
ADD CONSTRAINT "PK_training" PRIMARY KEY
(id_training);


--
-- TOC entry 2830 (class 2606 OID 29862)
-- Name: users PK_users; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.users
ADD CONSTRAINT "PK_users" PRIMARY KEY
(id_users);


--
-- TOC entry 2834 (class 2606 OID 29864)
-- Name: users_access_menu PK_users_access_menu; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.users_access_menu
ADD CONSTRAINT "PK_users_access_menu" PRIMARY KEY
(id_users_access_menu);


--
-- TOC entry 2836 (class 2606 OID 29866)
-- Name: workex PK_workex; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.workex
ADD CONSTRAINT "PK_workex" PRIMARY KEY
(id_workex);


--
-- TOC entry 2832 (class 2606 OID 29868)
-- Name: users Unik_uname_email; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.users
ADD CONSTRAINT "Unik_uname_email" UNIQUE
(uname, email);


--
-- TOC entry 2824 (class 2606 OID 29870)
-- Name: spring_session_attributes spring_session_attributes_pk; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.spring_session_attributes
ADD CONSTRAINT spring_session_attributes_pk PRIMARY KEY
(session_primary_id, attribute_name);


--
-- TOC entry 2822 (class 2606 OID 29872)
-- Name: spring_session spring_session_pk; Type: CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.spring_session
ADD CONSTRAINT spring_session_pk PRIMARY KEY
(primary_id);


--
-- TOC entry 2818 (class 1259 OID 29873)
-- Name: spring_session_ix1; Type: INDEX; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE UNIQUE INDEX spring_session_ix1 ON public.spring_session USING btree
(session_id);


--
-- TOC entry 2819 (class 1259 OID 29874)
-- Name: spring_session_ix2; Type: INDEX; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE INDEX spring_session_ix2 ON public.spring_session USING btree
(expiry_time);


--
-- TOC entry 2820 (class 1259 OID 29875)
-- Name: spring_session_ix3; Type: INDEX; Schema: public; Owner: zzoqhhnacdntpd
--

CREATE INDEX spring_session_ix3 ON public.spring_session USING btree
(principal_name);


--
-- TOC entry 2839 (class 2606 OID 29876)
-- Name: applicants_access_job FK_applicants_access_job_applicants; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.applicants_access_job
ADD CONSTRAINT "FK_applicants_access_job_applicants" FOREIGN KEY
(id_ap) REFERENCES public.applicants
(id_ap) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2840 (class 2606 OID 29881)
-- Name: applicants_access_job FK_applicants_access_job_job; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.applicants_access_job
ADD CONSTRAINT "FK_applicants_access_job_job" FOREIGN KEY
(id_job) REFERENCES public.job
(id_job);


--
-- TOC entry 2841 (class 2606 OID 29886)
-- Name: certificate FK_certificate_applicants; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.certificate
ADD CONSTRAINT "FK_certificate_applicants" FOREIGN KEY
(id_ap) REFERENCES public.applicants
(id_ap) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2842 (class 2606 OID 29891)
-- Name: description FK_description_job; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.description
ADD CONSTRAINT "FK_description_job" FOREIGN KEY
(id_job) REFERENCES public.job
(id_job) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2843 (class 2606 OID 29896)
-- Name: educations FK_educations_applicants; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.educations
ADD CONSTRAINT "FK_educations_applicants" FOREIGN KEY
(id_ap) REFERENCES public.applicants
(id_ap) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2853 (class 2606 OID 29958)
-- Name: projects FK_projects_applicants; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.projects
ADD CONSTRAINT "FK_projects_applicants" FOREIGN KEY
(id_ap) REFERENCES public.applicants
(id_ap) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2844 (class 2606 OID 29901)
-- Name: qualification FK_qualification_job; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.qualification
ADD CONSTRAINT "FK_qualification_job" FOREIGN KEY
(id_job) REFERENCES public.job
(id_job) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2845 (class 2606 OID 29906)
-- Name: skills FK_skills_applicants; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.skills
ADD CONSTRAINT "FK_skills_applicants" FOREIGN KEY
(id_ap) REFERENCES public.applicants
(id_ap) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2847 (class 2606 OID 29911)
-- Name: submenu FK_submenu_menu; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.submenu
ADD CONSTRAINT "FK_submenu_menu" FOREIGN KEY
(id_menu) REFERENCES public.menu
(id_menu);


--
-- TOC entry 2848 (class 2606 OID 29916)
-- Name: training FK_training_applicants; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.training
ADD CONSTRAINT "FK_training_applicants" FOREIGN KEY
(id_ap) REFERENCES public.applicants
(id_ap) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2850 (class 2606 OID 29921)
-- Name: users_access_menu FK_users_access_menu_menu; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.users_access_menu
ADD CONSTRAINT "FK_users_access_menu_menu" FOREIGN KEY
(id_menu) REFERENCES public.menu
(id_menu) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2851 (class 2606 OID 29926)
-- Name: users_access_menu FK_users_access_menu_roles; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.users_access_menu
ADD CONSTRAINT "FK_users_access_menu_roles" FOREIGN KEY
(id_role) REFERENCES public.roles
(id_role);


--
-- TOC entry 2849 (class 2606 OID 29931)
-- Name: users FK_users_roles; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.users
ADD CONSTRAINT "FK_users_roles" FOREIGN KEY
(id_role) REFERENCES public.roles
(id_role);


--
-- TOC entry 2852 (class 2606 OID 29936)
-- Name: workex FK_workex_applicants; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.workex
ADD CONSTRAINT "FK_workex_applicants" FOREIGN KEY
(id_ap) REFERENCES public.applicants
(id_ap) ON
UPDATE CASCADE ON
DELETE CASCADE;


--
-- TOC entry 2846 (class 2606 OID 29941)
-- Name: spring_session_attributes spring_session_attributes_fk; Type: FK CONSTRAINT; Schema: public; Owner: zzoqhhnacdntpd
--

ALTER TABLE ONLY public.spring_session_attributes
ADD CONSTRAINT spring_session_attributes_fk FOREIGN KEY
(session_primary_id) REFERENCES public.spring_session
(primary_id) ON
DELETE CASCADE;


-- Completed on 2020-01-10 08:39:56

--
-- zzoqhhnacdntpdQL database dump complete
--

