----------------------------------------------------------------------
--SCHEMA AUTHENTICATION--
----------------------------------------------------------------------
INSERT INTO public.menu
VALUES 
(1,'Dashboard',1),
(2,'Dashboard Profesional',1),
(3,'Admin',1);

INSERT INTO public.submenu
VALUES 
(1,1,'GDP','/index2',NULL,1),
(2,1,'Detail GDP','/index/detail/1',NULL,1),
(3,2, 'Profesional','/index',NULL,1);

INSERT INTO public.roles
VALUES 
(1,'GDP'),
(2,'Profesional'),
(3,'Admin');

INSERT INTO public.users
VALUES 
(1,1,
'tegar.rahmanto',
'tegar.rahmanto@batmandiri.com',
'$2y$12$X5viXBBzro5TulhnAebR0uOQmKZN4tzMOGpICMt8KdH1ucCIwy0je',1),
(2,2,
'vinzha',
'vinzha@batmandiri.com',
'$2y$12$zYnBFuT0jBF.1552WwE8HOv2MlOsx.VXs4NFYHHIa/V28L6i5Uo4W',1),
(3,3,
'dewi',
'dewi@batmandiri.com',
'$2y$12$81u6OhZe8.5S9A5F9El80uuYOCGmeiXKJecJMRiw2.EyTkF373Pda',1);

INSERT INTO public.users_access_menu
VALUES 
(1,1,1),
(2,2,2),
(3,3,3);
----------------------------------------------------------------------
--SCHEMA APPLICANTS--
----------------------------------------------------------------------
INSERT INTO public.applicants (
    nama, alamat, email, contact, data_gambar, data_cv, link_gambar, link_cv, personal_statement
)
VALUES ('Bro',
'Jakarta',
'bro@gmail.com',
'085708120815',
NULL,NULL,NULL,NULL,
'Saya adalah anak yang baik');

INSERT INTO public.educations (
    id_ap, date_awal, date_akhir, school, major, description
)
VALUES (1,
'20160101',
'20190101',
'SMA 1 JAKARTA',
'SMA',
'Jurusan IPA');

INSERT INTO public.workex (
    id_ap, date_awal, date_akhir, job, description, company
)
VALUES (1,
'20171212',
'20181212',
'Programmer',
'Menjadi Junior Full Stack Developer',
'PT Barokah');

INSERT INTO public.skills (
    id_ap, skill, level
)
VALUES (1,'Java','Intermediate');

INSERT INTO public.training (
    id_ap, ntraining, date_awal, date_akhir, description, company
)
VALUES (1,
'Learn Java For Beginner',
'20160707',
'20170707',
'Belajar tentang bahasa pemrograman java',
'Arkademy');

INSERT INTO public.certificate (
    id_ap, nama_certificate, date, company, description
)
VALUES (1,
'Learn Java For Beginner',
'20170909',
'Arkademu',
'Belajar tentang bahasa pemrograman java'
);

INSERT INTO public.projects (
    id_ap, nama_project, posisi_project, date_awal, date_akhir
)
VALUES (1,
'Recruitment BATM',
'Developer',
'20191208',
'20200113');

----------------------------------------------------------------------------
--SCHEMA JOB--
----------------------------------------------------------------------
INSERT INTO public.job (
    nama_job, is_active
)
VALUES 
('IT Graduate Development Program',1),
('IT Project Manager',1),
('Visual Basic (VB) .Net Programmer',1),
('Cognos Developer',1),
('ETL Developer',1),
('IT Account Manager',1),
('Business Analyst - PSAK',1);

INSERT INTO public.description (
    id_job, nama_desc
)
VALUES 
(1,'Learn about programming languages ​​and databases.'),
(1,'Every weekly we will evaluate the training provided.'),
(2,'Leading Development Team'),
(2,'Project Budgeting Team'),
(2,'Communications Oral across levels'),
(2,'Written Communications'),
(2,'Ability to mentor Task Forces'),
(3,'Participate in requirement analysis'),
(3,'Collaborate with internal teams to produce software design'),
(3,'Write clean, scalable code using .NET Programming languages'),
(3,'Test and deploy applications and systems'),
(3,'Revise, update, refactor and debug code'),
(3,'Improve existing software'),
(3,'Develop documentation throughout the software development life cycle'),
(4,'Deliver specific data and reporting services to meet customer requirements, meeting regularly with key users and their managers to understand business needs and identify improvement opportunities.'),
(4,'Provide assistance to end users regarding utilization of IT systems & information to deliver business objectives.'),
(4,'Technical support for data and reports including: end-user support; monitoring scheduled reports and data loads; troubleshooting; and engagement with other teams and service providers.'),
(4,'Data Analysis activities such as requirements definition, documentation and testing'),
(4,'Report development and oning maintenance activities'),
(4,'Coordination of patch releases and change management.'),
(4,'Engagement with stakeholders to prioritise work requests and system improvement initiatives'),
(4,'Delivery of prioritised system improvements.'),
(4,'Apply, share and develop comprehensive skills and knowledge to deliver outcomes within specific IT data and reporting specializations.'),

(5,'Create reporting using ETL tools'),
(5,'Design, build and deploy effective SSIS packages'),
(5,'Implement stored procedures and effectively query a database'),
(5,'Identify and test for bugs and bottlenecks in the ETL solution'),

(6,'Meeting clients to discuss their advertising needs'),
(6,'Working with account planners to device a campaign that meets the client’s brief and budget'),
(6,'Presenting campaign ideas and costings to client'),
(6,'Briefing the creative team who will produce the adverts'),
(6,'Netiating with clients, solving any problems and making sure deadlines are met'),
(6,'Checking and reporting on the campaign’s progress'),
(6,'Keepng in contact with the client at all stages of the campaign'),
(6,'Managing the account’s budget and invoicing the client'),
(6,'Making “pitches” to win new business'),

(7,'Lead the Software Development Life Cycle, including analysis, design, development and testing'),
(7,'Translate business user requirement into functional and/or technical requirement'),
(7,'Create high quality functional documentation including requirements and test documentation'),
(7,'Create simulation to describe high level calculation & classification process that will be automatized in the project and present it to Business users and teams'),
(7,'Bridging the Gap Between Business user and Tech Teams'),
(7,'Liaise with technical team in the project implementation to meet functional requirements'),
(7,'Leading & mentoring project team'),
(7,'Estimate work content and achieving planned timelines'),
(7,'Do datamart data mapping and/or field to field mapping'),
(7,'Provides recommendation and solution including workaround solution to solve problem'),
(7,'Database scripting (SP, View, etc)')
;

INSERT INTO public.qualification (
    id_job, nama_qualification
)
VALUES 
(1,'Fresh Graduate from reputable University with a Degree of Information Technology / Computer Science.'),
(1,'Technical background with minimum GPA 3.00 of 4.00.'),
(1,'Has conceptual knowledge in relational database management system (RDBMS), such as Mysql, SQL Server, Oracle, etc.'),
(1,'Has od knowledge of programming language 1 skill, such as (JAVA, PHP / VB / C / C++ / .NET, Mobile (Android)); mandatory in Java Programming'),
(1,'od English communication skill both in written and verbal.'),

(2,'Bachelor’s Degree in Information Technology/Management'),
(2,'Have PMP certification or equivalent'),
(2,'3-5 years working successfully leading systems implementation in banking projects'),
(2,'Preferable also having knowledge or experience in infrastructure'),
(2,'Have experience in managing core banking systems implementation'),
(2,'Clear understanding of SDLC and agile methodologies'),
(2,'Having od knowledge/skill in application, data migration and infrastructure'),
(2,'Having experience in managing large sized project with multiple on-ing sub-projects simultaneously'),

(3,'Experience as .NET Developer'),
(3,'Application Developer Familiarity with the ASP.NET framework, SQL Server, Postgre SQL'),
(3,'Knowledge of Visual Basic .NETStrong analytical and planning skills'),
(3,'od communication and presentation skills'),
(3,'Excellent problem-solving skills'),

(4,'2 or more years of experience supporting Business Intelligence & Reporting software products including :'),
(4,'1. IBM Cognos BI Suite, IBM Cognos Planning'),
(4,'2. TM1 (Planning Analytics)'),
(4,'3. Power BI, Qlik Sense, Tableau or similar'),
(4,'Knowledge Enterprise Planning & Budgeting Process'),
(4,'Strong analytical and planning skills'),
(4,'od communication and presentation skills'),
(4,'Excellent problem-solving skills'),

(5,'Graduate Degree in Information Technology from reputable University'),
(5,'Has minimum 1 year of experience in MS. SQL Server or Oracle (PL/SQL) also have experiences in SSIS, ODI, Pentaho or any ETL Tools'),
(5,'Have knowledge on creating and altering Stored Procedure, Function, Trigger, Table, View, data base, etc.'),
(5,'od Analysis'),
(5,'Can work as a team'),
(5,'Quick learn'),

(6,'Bachelor’s Degree of Computer Science/ Information Techonology, Economics, Business Administration/ Management, Communication, or related'),
(6,'1 year of working experience in the related field in IT Consulting Company is an advantage'),
(6,'High Passion as Sales'),
(6,'Multitasking, hard-worker, able to work in team, and client oriented environment'),
(6,'Have oustanding analytical, od network of executives, especially in the Banking & Financial Service'),
(6,'Problem-solving, od oral-written communication in English'),
(6,'Able to work under pressure and target oriented'),

(7,'Candidate must possess at least Bachelor’s Degree, Master’s Degree/Post Graduate Degree in Engineering (Computer/Telecommunication), Computer Science/Information Technology or equivalent.'),
(7,'Minimum 2 – 4 years experience as Business Analyst or as an MIS reporting.'),
(7,'od knowledge of Indonesian'),
(7,'Regulatory Reporting (i.e. PSAK 50/55, LBU etc).'),
(7,'Able to write SQL queries / command.'),
(7,'Having experience in programming is an advantage.'),
(7,'Having experience with Macros, Pivot Tables, and Power-Pivot within Excel.'),
(7,'Having experience in leading a team & project is an advantage.'),
(7,'Having experience in Business Intelligence Reporting project is an advantage.'),
(7,'Able to work in team.'),
(7,'Able to work under pressure.')
;
----------------------------------------------------------------------
--PENGHUBUNG SCHEMA APPLICANTS DENGAN JOB--
----------------------------------------------------------------------
INSERT INTO public.applicants_access_job (
    id_job, id_ap, status
)
VALUES (1,1,1);